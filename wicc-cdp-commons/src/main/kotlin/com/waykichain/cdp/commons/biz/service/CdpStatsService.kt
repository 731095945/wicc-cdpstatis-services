package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.entity.domain.CdpStats

interface CdpStatsService {
    fun getById(id:Long): CdpStats?

    fun save(cdpStats:CdpStats): CdpStats
}
