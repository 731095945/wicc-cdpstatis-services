package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.util.*

class CdpStatVO {

    @ApiModelProperty("cdpAddition")
    var cdpAddition: Int? = null

    @ApiModelProperty("time")
    var time: Date? = null

    @ApiModelProperty("cdpClose")
    var cdpClose: Int? = null
}