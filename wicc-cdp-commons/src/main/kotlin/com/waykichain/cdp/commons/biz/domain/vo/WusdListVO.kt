package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class WusdListVO {
    var list = arrayListOf<WusdVO>()
}

class WusdVO {

    @ApiModelProperty("wusd当天generate")
    var wusdGenerated: BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wusd当天Burned")
    var wusdBurned: BigDecimal  = BigDecimal.ZERO

    @ApiModelProperty("时间")
    var time:  String? = null

    @ApiModelProperty("price")
    var price:  BigDecimal? = null


    @ApiModelProperty("wusd当天总共supply")
    var wusdTotalSupply: BigDecimal = BigDecimal.ZERO

}