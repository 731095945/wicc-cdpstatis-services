package com.waykichain.cdp.commons.biz.dict

enum class CdpLiquateType(val code: Int, val description: String) {
    PART(0, "部分清算"),
    TOTAL(1,"全部清算")
}