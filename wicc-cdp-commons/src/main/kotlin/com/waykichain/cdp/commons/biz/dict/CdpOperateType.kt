package com.waykichain.cdp.commons.biz.dict

/**
 * cdp 操作类型
 */
enum class CdpOperateType(val code: Int, val description: String){

    CREATE               (100, "创建"),

    PLEDGE               (200, "抵押"),

    LOAN_OUT             (300, "贷出"),

    PLEDGE_LOAN_OUT      (400, "抵押&贷出"),

    REPAY                (500, "偿还"),

    REDEEM               (600, "赎回"),

    REPAY_REDEEM         (700, "偿还&赎回"),

    CLOSE                (800, "关闭"),

    LIQUATE              (900, "清算")

}