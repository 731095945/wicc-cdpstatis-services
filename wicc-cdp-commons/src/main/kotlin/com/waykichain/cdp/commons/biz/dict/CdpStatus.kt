package com.waykichain.cdp.commons.biz.dict

/**
 *  Created by yehuan on 2019/7/15
 */

enum class CdpStatus(val code: Int, val description: String){

    NULL    (0, "未初始化"),

    OPENING (100, "没关闭"),

    SAFETY  (200, "正常"),
    UNSAFETY (300,"不正常"),

    CLOSED  (900, "已关闭的"),

    ALL     (1000, "全部")

}