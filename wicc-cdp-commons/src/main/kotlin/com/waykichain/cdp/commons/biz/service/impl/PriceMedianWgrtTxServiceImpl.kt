package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.service.PriceMedianWgrtTxService
import com.waykichain.cdp.entity.domain.PriceMedianWgrtTx
import com.waykichain.cdp.repository.PriceMedianWgrtTxRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class PriceMedianWgrtTxServiceImpl: PriceMedianWgrtTxService {

    override fun getById(id:Long): PriceMedianWgrtTx? {
        return priceMedianWgrtTxRepository.findOne(id)
    }

    override fun save(priceMedianWgrtTx:PriceMedianWgrtTx): PriceMedianWgrtTx {
        return priceMedianWgrtTxRepository.saveAndFlush(priceMedianWgrtTx)
    }

    @Autowired lateinit var priceMedianWgrtTxRepository: PriceMedianWgrtTxRepository
}
