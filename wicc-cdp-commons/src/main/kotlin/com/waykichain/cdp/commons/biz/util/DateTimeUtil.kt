package com.waykichain.cdp.commons.biz.util

import com.waykichain.cdp.commons.biz.dict.TimeIntervalType
import com.waykichain.cdp.commons.biz.domain.constants.CdpConstant
import com.waykichain.cdp.commons.biz.domain.po.CdpStatPO
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*
import java.util.ArrayList
import org.springframework.boot.test.mock.mockito.MockReset.before




/**
 * @Author: yanjunlin
 * @CreateDate: 2019/4/28 18:07
 * @Description: 时间工具类
 */
class DateTimeUtil {


    companion object {

        /**
         * 与当前时间的差值
         * timeZone-目标时间时区
         */
        fun timeDifferenceFromNow(date: Date): Int {
            try {
                if (date == null) return 0
                return ((Calendar.getInstance().timeInMillis-date.toInstant().toEpochMilli())/1000).toInt()
            } catch (e: Exception) {
                e.printStackTrace()
                return 0
            }
        }

        fun getDayOfLastMonth(): String {
            var calendar = Calendar.getInstance()
            var template: Format = SimpleDateFormat("yyyy-MM-dd")
            calendar.add(Calendar.MONTH, -1)
            return template.format(calendar.time)
        }

        fun getDayOfLastWeek(): String {
            var calendar = Calendar.getInstance()
            var template: Format = SimpleDateFormat("yyyy-MM-dd")
            calendar.add(Calendar.DAY_OF_WEEK, -7)
            return template.format(calendar.time)
        }

        fun getToday(): String {
            var template: Format = SimpleDateFormat("yyyy-MM-dd")
            return template.format(Date())
        }

        fun getDayOfStr(d: Date): String {
            var template: Format = SimpleDateFormat("yyyy-MM-dd")
            return template.format(d)
        }

        fun getDate(d: String): Date {
            var template = SimpleDateFormat("yyyy-MM-dd")
            return template.parse(d)
        }




        fun setStartEndTime(po: CdpStatPO) {
            when(po.timeIntervalType) {
                TimeIntervalType.ONE_MONTH.description -> {
                    po.startTime = DateTimeUtil.getDayOfLastMonth()
                    po.endTime = DateTimeUtil.getToday()
                }
                TimeIntervalType.ONE_WEEK.description -> {
                    po.startTime = DateTimeUtil.getDayOfLastWeek()
                    po.endTime = DateTimeUtil.getToday()
                }
                TimeIntervalType.ALL.description -> {
                    po.startTime = CdpConstant.CDP_ONLINE_TIME
                    po.endTime = DateTimeUtil.getToday()
                }
            }
        }

        @Throws(Exception::class)
        fun dateSplit(startDate: Date, endDate: Date): List<Date> {
            if (!startDate.before(endDate) && DateTimeUtil.getDayOfStr(startDate) != DateTimeUtil.getDayOfStr(endDate))
                throw Exception("开始时间应该在结束时间之后")
            val spi = endDate.time - startDate.time
            val step = (spi / (24 * 60 * 60 * 1000)).toInt()// 相隔天数

            val dateList = ArrayList<Date>()
            dateList.add(endDate)
            for (i in 1..step) {
                dateList.add(Date(dateList.get(i - 1).time - 24 * 60 * 60 * 1000))// 比上一天减一
            }
            return dateList
        }



    }



}

fun main(args: Array<String>) {
    val diff = DateTimeUtil.timeDifferenceFromNow(Date(1556454792000))
    println(diff)
}