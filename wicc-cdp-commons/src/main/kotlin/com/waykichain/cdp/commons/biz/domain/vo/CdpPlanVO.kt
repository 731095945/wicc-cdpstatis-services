package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty

class CdpPlanVO: CdpDetailVO() {

//    @ApiModelProperty("数据库id")
//    var id: Long? = null
//
//    @ApiModelProperty("抵押率")
//    var collateralRate: String? = null

    @ApiModelProperty("当前的清算费用")
    var currentLdValue: String? = null

    @ApiModelProperty("清算费用上限")
    var maxLdValue: String? = null

    @ApiModelProperty("清算折扣费")
    var ldDiscount: String? = null

    @ApiModelProperty("小费")
    var fees: String? = null

    @ApiModelProperty("资产总量")
    var assets: String? = null

}