package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.service.DelegateVoteWgrtTxService
import com.waykichain.cdp.entity.domain.DelegateVoteWgrtTx
import com.waykichain.cdp.entity.domain.QCdpTx
import com.waykichain.cdp.entity.domain.QDelegateVoteWgrtTx
import com.waykichain.cdp.repository.DelegateVoteWgrtTxRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import java.math.BigDecimal

@Service
class DelegateVoteWgrtTxServiceImpl: DelegateVoteWgrtTxService {

    override fun getById(id:Long): DelegateVoteWgrtTx? {
        return delegateVoteWgrtTxRepository.findOne(id)
    }

    override fun save(delegateVoteWgrtTx:DelegateVoteWgrtTx): DelegateVoteWgrtTx {
        return delegateVoteWgrtTxRepository.saveAndFlush(delegateVoteWgrtTx)
    }

    override fun queryLastVoteTime(addr:String):Long {
        var predicate = QDelegateVoteWgrtTx.delegateVoteWgrtTx.fromAddr.eq(addr)
        val sort = Sort(Sort.Direction.DESC, "id")
        val pageRequest = PageRequest(0,1, sort)
        val tx =  delegateVoteWgrtTxRepository.findAll(predicate, pageRequest).firstOrNull()
        return if(tx==null)0 else tx.confirmedTime
    }

    fun getWgrtIssueAmount(day: String): BigDecimal{
        return delegateVoteWgrtTxRepository.getWgrtIssueAmount(day)
    }


    @Autowired lateinit var delegateVoteWgrtTxRepository: DelegateVoteWgrtTxRepository
}
