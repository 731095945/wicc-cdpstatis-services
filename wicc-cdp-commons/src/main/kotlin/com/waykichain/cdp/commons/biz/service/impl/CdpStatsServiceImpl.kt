package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.service.CdpStatsService
import com.waykichain.cdp.entity.domain.CdpStats
import com.waykichain.cdp.repository.CdpStatsRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class CdpStatsServiceImpl: CdpStatsService {

    override fun getById(id:Long): CdpStats? {
        return cdpStatsRepository.findOne(id)
    }

    override fun save(cdpStats:CdpStats): CdpStats {
        return cdpStatsRepository.saveAndFlush(cdpStats)
    }

    @Autowired lateinit var cdpStatsRepository: CdpStatsRepository
}
