package com.waykichain.cdp.commons.biz.domain.vo

import com.waykichain.chain.vo.v2.BasePageableVO
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal
import java.util.*

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/25 16:20
 *
 * @Description:    cdp transactions
 *
 */
class CdpTxListVO: BasePageableVO() {

    var list = arrayListOf<CdpTxDetailVO>()

    @ApiModelProperty("类型 0-全部 1-当前cdp操作记录 2-历史cdp操作记录")
    var type: Int = 0
}

class CdpTxDetailVO {

    @ApiModelProperty("id")
    var id: Long? = null

    @ApiModelProperty("交易哈希")
    var txHash: String? = null

    @ApiModelProperty("交易类型: 100-创建 200-抵押 300-贷出 400-抵押&贷出 500-偿还 600-赎回 700-偿还&赎回 800关闭 900清算")
    var txType: Int? = null

    @ApiModelProperty("链上唯一标识")
    var cdpId: String? = null

    @ApiModelProperty("区块高度")
    var blockHeight: Int? = null

    @ApiModelProperty("利息（wusd）")
    var scoinInterest: String? = null

    @ApiModelProperty("稳定币数量（wusd）")
    var scoinAmount: String? = null

    @ApiModelProperty("抵押币数量（wicc）")
    var bcoinAmount: String? = null

    @ApiModelProperty("罚金（wusd）")
    var scoinPenalty: String? = null

    @ApiModelProperty("小费")
    var fees: String? = null

    @ApiModelProperty("小费单位")
    var feeSymbol: String? = null

    @ApiModelProperty("交易前wicc数量")
    var fromWiccAmount: String? = null

    @ApiModelProperty("交易前wusd数量")
    var fromWusdAmount: String? = null

    @ApiModelProperty("交易后wicc数量")
    var toWiccAmount: String? = null

    @ApiModelProperty("交易后wusd数量")
    var toWusdAmount: String? = null

    @ApiModelProperty("交易前cdp状态")
    var fromStatus: Int? = null

    @ApiModelProperty("交易后cdp状态")
    var toStatus: Int? = null

    @ApiModelProperty("创建时间")
    var createdAt: Date? = null

    @ApiModelProperty("修改时间")
    var updatedAt: Date? = null

    @ApiModelProperty("清算资产量(WICC)")
    var assertValue: String? = null

    @ApiModelProperty("清算资产量($)")
    var assertValueDollar: String? = null

    @ApiModelProperty("清算付出")
    var payValue: String? = null

    @ApiModelProperty("抵押率", hidden = true)
    var collateralRate: String? = null

    @ApiModelProperty("链上确认时间")
    var confirmedAtMs: Long? = null


}