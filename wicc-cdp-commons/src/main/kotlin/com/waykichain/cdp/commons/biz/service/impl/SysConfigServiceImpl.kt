package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.service.SysConfigService
import com.waykichain.cdp.entity.domain.QSysConfig
import com.waykichain.cdp.entity.domain.SysConfig
import com.waykichain.cdp.repository.SysConfigRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class SysConfigServiceImpl: SysConfigService {

    override fun getById(id:Long): SysConfig? {
        return sysConfigRepository.findOne(id)
    }

    override fun save(sysConfig:SysConfig): SysConfig {
        return sysConfigRepository.saveAndFlush(sysConfig)
    }

    override fun getValueByName(name: String): String? {
        val wiccSysConfig = sysConfigRepository.findOne(QSysConfig.sysConfig.name.eq(name))
        return wiccSysConfig?.value
    }

    @Autowired lateinit var sysConfigRepository: SysConfigRepository
}
