package com.waykichain.cdp.commons.biz.domain.po

import com.waykichain.chain.po.v2.BasePageablePO
import io.swagger.annotations.ApiModelProperty
import org.hibernate.validator.constraints.NotBlank
import javax.validation.constraints.Min

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/25 17:54
 *
 * @Description:    cdp交易记录
 *
 */
open class CdpBasePageablePO() {
    @ApiModelProperty(value = "当前页，从1开始（默认1）", example = "1")
    var currentpage: Int = 1

    @ApiModelProperty(value="每页条数（默认10）", example = "10")
    @Min(value=1,message = "[in param error] Minimum value of pageSize is 1")
    var pagesize: Int = 10
}

class CdpTxListPO: CdpBasePageablePO() {

    @ApiModelProperty("链上唯一标识", hidden = true)
    var cdpId: String? = null

    @ApiModelProperty("链上唯一标识", hidden = true)
    var cdpIds = kotlin.collections.arrayListOf<String>()

    @ApiModelProperty("用户地址")
//    @NotBlank(message = "[in param error] address is null")
    var address: String? = null

    @ApiModelProperty("类型 0-所有 1-当前cdp操作记录 2-历史cdp操作记录", example = "1")
    var type: Int = 1

}