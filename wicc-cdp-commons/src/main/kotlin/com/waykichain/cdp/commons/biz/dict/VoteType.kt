package com.waykichain.cdp.commons.biz.dict

enum class VoteType (val code: Int, val description: String){
    ADD_BCOIN(100, "ADD_BCOIN"),
    MINUS_BCOIN(200, "MINUS_BCOIN")

}