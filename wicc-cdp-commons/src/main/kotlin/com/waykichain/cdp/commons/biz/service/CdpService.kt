package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.commons.biz.domain.po.CdpLdListPO
import com.waykichain.cdp.commons.biz.domain.po.CdpLdTxListPO
import com.waykichain.cdp.commons.biz.domain.po.CdpListPO
import com.waykichain.cdp.entity.domain.Cdp
import org.springframework.data.domain.Page
import java.math.BigDecimal

interface CdpService {
    fun getById(id: Long): Cdp?

    fun getByHash(hash: String): Cdp?

    fun getByOwnerAddress(ownerAddr: String): List<Cdp>

    fun getByOwnerAddressAndStatus(ownerAddr: String, status: Int?): Cdp?

    fun getLastCloseCdp(ownerAddr: String, status: Int?): Cdp?

    fun save(wiccCdp: Cdp): Cdp

    fun saveList(list: List<Cdp>)

    fun queryByStatus(po: CdpListPO): Page<Cdp>

    fun queryCloseCdpList(po: CdpLdTxListPO): Page<Cdp>

    fun queryByPageAndLdCR(po: CdpLdListPO): Page<Cdp>

    fun updateCdpStatus(price: BigDecimal)

    fun queryUnsafeCdpList(price: BigDecimal): List<Cdp>
}