package com.waykichain.cdp.commons.biz.domain.po

import com.waykichain.chain.po.v2.BasePageablePO
import io.swagger.annotations.ApiModelProperty

class CdpLdTxListPO:  BasePageablePO() {
    @ApiModelProperty("用户地址")
    var address: String? = null
}