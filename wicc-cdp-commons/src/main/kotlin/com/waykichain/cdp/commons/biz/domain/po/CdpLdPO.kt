package com.waykichain.cdp.commons.biz.domain.po

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class CdpLdPO {
    @ApiModelProperty("cdp 数据库id")
    var id: Long? = null
}