package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.entity.domain.DelegateVoteWgrtTx

interface DelegateVoteWgrtTxService {
    fun getById(id:Long): DelegateVoteWgrtTx?

    fun save(delegateVoteWgrtTx:DelegateVoteWgrtTx): DelegateVoteWgrtTx

    fun queryLastVoteTime(addr:String):Long


}
