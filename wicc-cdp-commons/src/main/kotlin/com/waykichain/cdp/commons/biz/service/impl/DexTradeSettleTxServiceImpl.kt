package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.service.DexTradeSettleTxService
import com.waykichain.cdp.entity.domain.DexTradeSettleTx
import com.waykichain.cdp.repository.DexTradeSettleTxRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class DexTradeSettleTxServiceImpl: DexTradeSettleTxService {

    override fun getById(id:Long): DexTradeSettleTx? {
        return dexTradeSettleTxRepository.findOne(id)
    }

    override fun save(dexTradeSettleTx:DexTradeSettleTx): DexTradeSettleTx {
        return dexTradeSettleTxRepository.saveAndFlush(dexTradeSettleTx)
    }

    @Autowired lateinit var dexTradeSettleTxRepository: DexTradeSettleTxRepository
}
