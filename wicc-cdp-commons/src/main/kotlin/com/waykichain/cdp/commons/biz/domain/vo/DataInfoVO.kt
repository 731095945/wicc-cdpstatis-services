package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty

class DataInfoVO {

//    @ApiModelProperty("wusd每日产生和销毁")
//    var wusdListVO: WusdListVO ?= null
//
//    @ApiModelProperty("wicc每日抵押和赎回")
//    var wiccListVO: WiccListVO ?= null
//
//    @ApiModelProperty("cdp当天增加数,cdp当天关闭数")
//    var cdpStatisticsListVO: CdpStatisticsListVO ?= null

    @ApiModelProperty("cdp/wicc/wusd/wgrt list")
    var statisticsList = arrayListOf<StatisVO>()

    @ApiModelProperty("current supply wusd,wusd generated/burned in 24 hours")
    var wusdInfoVO: WusdInfoVO = WusdInfoVO()

    @ApiModelProperty("current supply wicc,wicc generated/burned in 24 hours")
    var wiccInfoVO: WiccInfoVO = WiccInfoVO()

    @ApiModelProperty("global CR,global active cdps,addtional/closed cdps in 24h")
    var cdpInfoVO :CdpInfoVO = CdpInfoVO()

    @ApiModelProperty("wgrt Issue Amount,wgrt change rate in 24h, wgrt total burned/issue")
    var wgrtInfo: WgrtInfoVO = WgrtInfoVO()




}