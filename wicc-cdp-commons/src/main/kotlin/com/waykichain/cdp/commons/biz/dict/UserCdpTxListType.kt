package com.waykichain.cdp.commons.biz.dict

/**
 *  cdp操作列表类型
 */
enum class UserCdpTxListType(val code: Int, val description: String){

    ALL      (0, "所有"),

    PRESENT  (1, "当前"),

    HISTORY  (2, "历史")

}