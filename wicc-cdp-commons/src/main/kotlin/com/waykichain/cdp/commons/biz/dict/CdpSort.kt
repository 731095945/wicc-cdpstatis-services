package com.waykichain.cdp.commons.biz.dict

enum class CdpSort(val code: Int, val field: String) {
    ID(1, "id"),
    COLLATERALRATE(2, "collateralRate"),
    WICC(3, "bcoinAmount"),
    WUSD(4, "scoinAmount"),
    LDASSERT(5, "liquidationAssert"),
    LDFEE(6, "liquidationFee")
}
