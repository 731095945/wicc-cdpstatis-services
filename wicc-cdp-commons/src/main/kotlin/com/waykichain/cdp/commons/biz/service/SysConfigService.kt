package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.entity.domain.QSysConfig
import com.waykichain.cdp.entity.domain.SysConfig

interface SysConfigService {
    fun getById(id:Long): SysConfig?
    fun getValueByName(name: String): String?
    fun save(sysConfig:SysConfig): SysConfig
}
