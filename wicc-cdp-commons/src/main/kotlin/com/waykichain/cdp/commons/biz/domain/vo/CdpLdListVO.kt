package com.waykichain.cdp.commons.biz.domain.vo

import com.waykichain.chain.vo.v2.BasePageableVO
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class CdpLdListVO: BasePageableVO() {

    var list = arrayListOf<CdpLdDetailVO>()
}


class CdpLdDetailVO {

    @ApiModelProperty("资产价值")
    var assertValue: BigDecimal? = null

    @ApiModelProperty("折扣")
    var discout: Double? = null

    @ApiModelProperty("cdp详情")
    var cdpInfo: CdpDetailVO? = null
}