package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class WiccInfoVO {
    @ApiModelProperty("current supply wusd")
    var wiccLocked: BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wicc addition in 24 hours")
    var wiccAddition:  String  = "0"

    @ApiModelProperty("wicc redemption in 24 hours")
    var redemption:  String  = "0"
}