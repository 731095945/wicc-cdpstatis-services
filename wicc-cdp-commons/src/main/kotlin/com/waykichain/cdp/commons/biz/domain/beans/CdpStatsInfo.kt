package com.waykichain.cdp.commons.biz.domain.beans

import java.math.BigDecimal

/**
 *  Created by yehuan on 2019/7/16
 */

class CdpStatsInfo{

    var totalWiccAmount = BigDecimal.ZERO

    var totalWusdAmount = BigDecimal.ZERO

    var totalCdpAmount  = 0

    var forceLiquidateCdpAmount = 0L

    var forceLiquidateRatio: String? = null

    var globalCollateralCeiling = 0L

    var globalCollateralRatioFloor: String? = null

    var globalCollateralRatioFloorReached :Boolean = false

    var height = 0L

    var slideWindowBlockCount = 0

    var totalCollateralRatio: String = ""
}