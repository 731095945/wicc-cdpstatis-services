package com.waykichain.cdp.commons.biz.util

import java.math.BigDecimal

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/30 17:22
 *
 * @Description:
 *
 */
object AmountOperateUtil {

    fun getAmountFromLong(amount: Long?): String{
        amount?: return BigDecimal.ZERO.toPlainString()
        return BigDecimal(amount).divide(BigDecimal("100000000"), 8,BigDecimal.ROUND_DOWN).stripTrailingZeros().toPlainString()
    }

    fun getAmountFromBigDecimal(amount: BigDecimal?): String{
        amount?: return BigDecimal.ZERO.toPlainString()
        return amount.divide(BigDecimal("100000000"), 8,BigDecimal.ROUND_DOWN).stripTrailingZeros().toPlainString()
    }

    fun getPriceFromBigDec(amount: BigDecimal?): BigDecimal{
        amount?: return BigDecimal.ZERO
        return (amount).divide(BigDecimal("10000"), 8, BigDecimal.ROUND_UP)
    }

}