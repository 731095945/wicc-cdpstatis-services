package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.entity.domain.MedianPrice

interface MedianPriceService {
    fun getById(id:Long): MedianPrice?

    fun save(medianPrice:MedianPrice): MedianPrice
}
