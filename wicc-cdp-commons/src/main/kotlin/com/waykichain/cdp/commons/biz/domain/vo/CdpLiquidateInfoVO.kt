package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class CdpLiquidateInfoVO {

    @ApiModelProperty("清算时间")
    var liquidateTime:Long = 0L

    @ApiModelProperty("清算交易hash")
    var hash:String ?= null

    @ApiModelProperty("清算类型（0：部分清算，1：全部清算）")
    var type:Int?= null

    @ApiModelProperty("cdp id")
    var cdpId:String?= null

    @ApiModelProperty("返回抵押人WICC金额")
    var amount:BigDecimal?= null

}