package com.waykichain.cdp.commons.biz.domain.vo

import com.waykichain.cdp.commons.biz.domain.constants.CdpConstant
import io.swagger.annotations.ApiModelProperty

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/24 16:41
 *
 * @Description:    $des
 *
 */
class CdpGlobalInfoVO {

    @ApiModelProperty("wicc价格")
    lateinit var wiccPrice: String

    @ApiModelProperty("wusd价格")
    lateinit var wusdPrice: String

    @ApiModelProperty("cdp全局抵押数量")
    var cdpTotalWiccAmount = CdpConstant.ZERO_STRING

    @ApiModelProperty("cdp全局贷出数量")
    var cdpTotalWusdAmount = CdpConstant.ZERO_STRING

    @ApiModelProperty("cdp全局抵押率")
    var cdpTotalCollateralRatio = CdpConstant.ZERO_STRING

    @ApiModelProperty("正常运行中的cdp数量")
    var cdpTotalCount = 0

    @ApiModelProperty("清算率")
    var liquateRate = 0

    @ApiModelProperty("清算罚金")
    var penalty =0f

    @ApiModelProperty("稳定费")
    var wusdFee = 0f
}