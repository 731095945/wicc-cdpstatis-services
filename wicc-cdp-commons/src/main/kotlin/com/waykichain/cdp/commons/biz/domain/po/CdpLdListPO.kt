package com.waykichain.cdp.commons.biz.domain.po

import com.waykichain.chain.po.v2.BasePageablePO
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class CdpLdListPO:  BasePageablePO() {

    @ApiModelProperty("wicc 价格",hidden = true)
    var price: BigDecimal? = null

    @ApiModelProperty("排序字段,1:CDP ID,2:抵押率,3:资产总量,4:债务总量,5:待清算资产量，6:清算需付出")
    var sortFieldId: Int? = null

    @ApiModelProperty("desc or asc")
    var sortOrder: String? = null
}