package com.waykichain.cdp.commons.biz.domain.vo

import com.waykichain.cdp.commons.biz.domain.constants.CdpConstant
import io.swagger.annotations.ApiModelProperty

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/24 16:06
 *
 * @Description:    用户详情
 *
 */
class UserInfoDetailVO {

    @ApiModelProperty("用户地址")
    lateinit var address: String

    @ApiModelProperty("wicc数量")
    var wiccAmount= CdpConstant.ZERO_STRING

    @ApiModelProperty("wusd数量")
    var wusdAmount = CdpConstant.ZERO_STRING

    @ApiModelProperty("cdp详情")
    var cdpInfo: CdpDetailVO? = null

    @ApiModelProperty("old cdp详情")
    var oldCdpInfo: CdpDetailVO? = null

    @ApiModelProperty("清算信息")
    var liquidateInfo: CdpLiquidateInfoVO?= null

}