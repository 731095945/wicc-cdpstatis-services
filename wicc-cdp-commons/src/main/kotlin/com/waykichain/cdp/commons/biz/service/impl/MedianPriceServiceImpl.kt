package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.service.MedianPriceService
import com.waykichain.cdp.entity.domain.MedianPrice
import com.waykichain.cdp.repository.MedianPriceRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class MedianPriceServiceImpl: MedianPriceService {

    override fun getById(id:Long): MedianPrice? {
        return medianPriceRepository.findOne(id)
    }

    override fun save(medianPrice:MedianPrice): MedianPrice {
        return medianPriceRepository.saveAndFlush(medianPrice)
    }

    @Autowired lateinit var medianPriceRepository: MedianPriceRepository
}
