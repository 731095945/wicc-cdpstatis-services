package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.dict.CdpSort
import com.waykichain.cdp.commons.biz.dict.CdpStatus
import com.waykichain.cdp.commons.biz.domain.constants.CdpConstant
import com.waykichain.cdp.commons.biz.domain.po.CdpLdListPO
import com.waykichain.cdp.commons.biz.domain.po.CdpLdTxListPO
import com.waykichain.cdp.commons.biz.domain.po.CdpListPO
import com.waykichain.cdp.commons.biz.service.CdpService
import com.waykichain.cdp.entity.domain.Cdp
import com.waykichain.cdp.entity.domain.QCdp
import com.waykichain.cdp.repository.CdpRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class CdpServiceImpl: CdpService {

    override fun queryUnsafeCdpList(price: BigDecimal): List<Cdp> {
        return cdpRepository.queryUnsafeCdpList(price)
    }

    override fun updateCdpStatus(price: BigDecimal) {
        cdpRepository.updateCdpStatus(price)
    }

    override fun getLastCloseCdp(ownerAddr: String, status: Int?): Cdp? {
        var predicate = QCdp.cdp.ownerAddr.eq(ownerAddr)
        if (status != null) predicate = predicate.and(QCdp.cdp.status.eq(status))
        return cdpRepository.findAll(predicate).maxBy { it.id }
    }


    override fun getByHash(hash: String): Cdp? {
        val predicate = QCdp.cdp.cdpId.eq(hash)
        return cdpRepository.findAll(predicate).firstOrNull()
    }

    override fun getByOwnerAddress(ownerAddr: String): List<Cdp> {
        return cdpRepository.findAll(QCdp.cdp.ownerAddr.eq(ownerAddr)).toList()
    }

    override fun getByOwnerAddressAndStatus(ownerAddr: String, status: Int?): Cdp? {
        var predicate = QCdp.cdp.ownerAddr.eq(ownerAddr)
        if (status != null) predicate = predicate.and(QCdp.cdp.status.eq(status))
        return cdpRepository.findAll(predicate).firstOrNull()
    }

    override fun getById(id:Long): Cdp? {
        return cdpRepository.findOne(id)
    }

    override fun save(cdp: Cdp): Cdp {
        return cdpRepository.saveAndFlush(cdp)
    }

    override fun saveList(list:List<Cdp>){
         cdpRepository.save(list)
    }


    override fun queryByStatus(po: CdpListPO): Page<Cdp> {
        val sort = Sort(Sort.Direction.DESC, "updatedAt")
        val pageRequest = PageRequest(po.currentpage-1, po.pagesize, sort)
        var predicate =   QCdp.cdp.status.eq(CdpStatus.OPENING.code)
        when(po.status) {
            CdpStatus.SAFETY.code -> predicate  =  QCdp.cdp.liquidatePrice.lt(po.price).and(QCdp.cdp.status.eq(CdpStatus.OPENING.code))
            CdpStatus.UNSAFETY.code -> predicate = QCdp.cdp.liquidatePrice.goe(po.price).and(QCdp.cdp.status.eq(CdpStatus.OPENING.code))
            CdpStatus.CLOSED.code -> predicate = QCdp.cdp.status.eq(CdpStatus.CLOSED.code)
            CdpStatus.ALL.code -> return  cdpRepository.findAll(pageRequest)
            CdpStatus.OPENING.code -> predicate = QCdp.cdp.status.eq(CdpStatus.OPENING.code)
        }
        val page = cdpRepository.findAll(predicate, pageRequest)
        return page
    }

    override fun queryCloseCdpList(po: CdpLdTxListPO): Page<Cdp> {
        val sort = Sort(Sort.Direction.DESC, "updatedAt")
        val pageRequest = PageRequest(po.currentpage - 1, po.pagesize, sort)
        val predicate = QCdp.cdp.status.eq(CdpStatus.CLOSED.code).and(QCdp.cdp.ownerAddr.eq(po.address))
        val page = cdpRepository.findAll(predicate, pageRequest)
        return page
    }

    override fun queryByPageAndLdCR(po: CdpLdListPO): Page<Cdp> {
        val sortFieldId = po.sortFieldId
        var sortFiled = "id"
        when(sortFieldId) {
            CdpSort.ID.code -> sortFiled = CdpSort.ID.field
            CdpSort.COLLATERALRATE.code -> sortFiled = CdpSort.COLLATERALRATE.field
            CdpSort.WICC.code -> sortFiled = CdpSort.WICC.field
            CdpSort.WUSD.code -> sortFiled = CdpSort.WUSD.field
            CdpSort.LDASSERT.code -> sortFiled = CdpSort.WUSD.field
            CdpSort.LDFEE.code -> sortFiled = CdpSort.WUSD.field
        }
        val predicate = QCdp.cdp.liquidatePrice.goe(po.price)
                .and(QCdp.cdp.liquidatePrice.lt(CdpConstant.CDP_MARKET_COLLATERAL_RATIO_3.divide(CdpConstant.CDP_MARKET_COLLATERAL_RATIO_1,4, BigDecimal.ROUND_HALF_UP).multiply(po.price)))
                .and(QCdp.cdp.status.eq(CdpStatus.OPENING.code))
        var sort = Sort(Sort.Direction.DESC, sortFiled)
        if(po.sortOrder == "desc")sort = Sort(Sort.Direction.DESC, sortFiled)
        else if(po.sortOrder == "asc")sort = Sort(Sort.Direction.ASC, sortFiled)
        val pageRequest = PageRequest(po.currentpage-1, po.pagesize, sort)
        val page = cdpRepository.findAll(predicate, pageRequest)
        return page
    }

    @Autowired lateinit var cdpRepository: CdpRepository
}
