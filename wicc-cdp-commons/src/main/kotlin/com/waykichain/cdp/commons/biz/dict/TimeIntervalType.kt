package com.waykichain.cdp.commons.biz.dict

enum class TimeIntervalType(val code: Int, val description: String) {

    ONE_MONTH   (0, "ONE_MONTH"),

    ONE_WEEK    (1, "ONE_WEEK"),

    ALL         (2, "ALL")


}