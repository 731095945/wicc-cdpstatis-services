package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

//class StatisListVO {
//    var list = arrayListOf<StatisVO>()
//}

class StatisVO {
    @ApiModelProperty("add")
    var add: String  = "0"

    @ApiModelProperty("minus")
    var minus:  String = "0"

    @ApiModelProperty("time")
    var time:  String? = null

    @ApiModelProperty("price")
    var price: BigDecimal? = null

    @ApiModelProperty("sum")
    var sum: BigDecimal? = null
}
