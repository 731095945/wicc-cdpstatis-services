package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.entity.domain.PriceMedianWgrtTx

interface PriceMedianWgrtTxService {
    fun getById(id:Long): PriceMedianWgrtTx?

    fun save(priceMedianWgrtTx:PriceMedianWgrtTx): PriceMedianWgrtTx
}
