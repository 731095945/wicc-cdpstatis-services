package com.waykichain.cdp.commons.biz.dict

enum class CdpCloseType (val code: Int, val description: String){
//    redeem ,manual_liquidate ,force_liquidate
    REDEEM(100,"redeem"),
    MANUAL_LIQUIDATE(200, "manual_liquidate"),
    FORCE_LIQUIDATE(300,"force_liquidate")
}