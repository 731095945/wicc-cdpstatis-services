package com.waykichain.cdp.commons.biz.domain.constants

import java.math.BigDecimal

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/24 18:00
 *
 * @Description:    $des
 *
 */
object CdpConstant {

    const val ZERO_STRING = "0"

    const val BALANCE_DECIMAL_NUMBER_8 = 8

    const val BALANCE_DECIMAL_NUMBER_4 = 4

    const val BALANCE_DECIMAL_NUMBER_2 = 2

    const val ONE_DAY_MILLISECOND_VALUE = 24*60*60*1000L
    const val BLOCK_COUNT = 28800

    val BIGDECIMAL_ONE_YEAR_365_DAY = 365.toBigDecimal()

    val BIGDECIMAL_ONE_DAY_MILLISECOND_VALUE = ONE_DAY_MILLISECOND_VALUE.toBigDecimal()

    val BIGDECIMAL_10_POW_8 = BigDecimal("100000000")

    val BIGDECIMAL_10_POW_4 = BigDecimal("10000")

    val BIGDECIMAL_ONE_HUNDRED = BigDecimal("100")


    /** wusd price (usd)*/
    const val WUSD_PRICE_USD = "1"


    /** cdp config*/
    val CDP_MARKET_COLLATERAL_RATIO_1 = BigDecimal("1.04")

    val CDP_MARKET_COLLATERAL_RATIO_2 = BigDecimal("1.13")

    val CDP_MARKET_COLLATERAL_RATIO_3 = BigDecimal("1.50")

    val CDP_MARKET_DISCOUNT = 0.97

    val CDP_OPEN_STATUS = 100

    val CDP_CLOSE_STATUS = 900

    val CDP_MARKET_FEE = 0.05

    //cdp稳定费
    val CDP_STABLE_FEE_CONSTANT = 0.2

    val CDP_STABLE_FEE_A = 2
    val CDP_STABLE_FEE_B = 1

    val CDP_SCOIN_FROM_LIQUIDATOR = 440
    val CDP_ASSET_TO_LIQUIDATOR = 441
    val CDP_LIQUIDATED_ASSET_TO_OWNER = 442
    val CDP_PENALTY_TO_RISK_RISERVE = 444
    val CDP_PENALTY_BUY_DEFLATE_FCOINS = 445
    val CDP_TOTAL_CLOSEOUT_SCOIN_FROM_RESERVE = 460
    val CDP_TOTAL_INFLATE_FCOIN_TO_RESERVE = 461
    val CDP_LIQUIDATED_CLOSEOUT_SCOIN = 443
    val CDP_TOTAL_ASSET_TO_RESERVE = 462
    val CDP_INTEREST_SCOINS_TO_FCOINS = 403
    val CDP_REDEEMED_ASSET_TO_OWNER = 421
    val CDP_REPAID_SCOIN_FROM_OWNER = 420


    val CDP_ONLINE_TIME = "2018-09-01"

    val CDP_WGRT_TOTAL_ISSUE = BigDecimal(21000000000).multiply(BigDecimal(0.96))

    val GLOBAL_COLLATERAL_RATIO_FLOOR = BigDecimal(1.04)


}