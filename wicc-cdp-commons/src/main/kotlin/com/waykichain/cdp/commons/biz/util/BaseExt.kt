package com.waykichain.cdp.commons.biz.util

import com.alibaba.fastjson.JSONObject
import com.waykichain.commons.util.WiccUtils
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

/**
 *  Created by yehuan on 2019/4/17
 */

fun  Any.toJSONString(): String{
    return JSONObject.toJSONString(this)
}


fun transGmt8toGmt0(dateofGmt8: Date): Date{
    return Date(dateofGmt8.time-8*60*60*1000)
}

fun WiccUtils.convertPrice(price: Long): BigDecimal = price.toBigDecimal().divide(BigDecimal(100_00)).setScale(4)
fun WiccUtils.convertPrice(price: BigDecimal): Long = price
        .multiply(BigDecimal(100_00)).toLong()
val yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"
fun timeZoneTransfer(date: Date, targetTimeZone: String): Date {
    val simpleDateFormat = SimpleDateFormat(yyyyMMddHHmmss)
    simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT$targetTimeZone")
    return simpleDateFormat.parse(simpleDateFormat.format(date))
}

fun <T> Iterable<T>.sumByBigDecimal(selector: (T) -> BigDecimal): BigDecimal {
    var sum = BigDecimal.ZERO
    for (element in this) {
        sum = sum.add(selector(element))
    }
    return sum
}
