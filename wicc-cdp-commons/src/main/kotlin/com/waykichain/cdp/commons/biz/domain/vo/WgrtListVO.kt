package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class WgrtListVO {
    var list = arrayListOf<WgrtVO>()
}

class WgrtVO {
    @ApiModelProperty("wgrt当天issue")
    var wgrtIssueAmount: BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wgrt当天burn")
    var wgrtBurnAmount:  BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("time")
    var time:  String? = null

    @ApiModelProperty("wgrt price")
    var price:  BigDecimal = BigDecimal.ZERO
}