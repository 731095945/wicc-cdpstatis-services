package com.waykichain.cdp.commons.biz.domain.vo

import com.waykichain.chain.vo.v2.BasePageableVO
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal
import java.util.*

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/24 15:35
 *
 * @Description:    用户cdp详情
 *
 */
class CdpListVO: BasePageableVO() {
    var list = arrayListOf<CdpDetailVO>()
}

open class CdpDetailVO {

    @ApiModelProperty("数据库id")
    var id: Long? = null

    @ApiModelProperty("抵押率")
    var collateralRate: String? = "0"

    @ApiModelProperty("cdp链上唯一标识")
    var cdpId: String? = null

    @ApiModelProperty("状态：100-正常 900-关闭")
    var status: Int? = null

    @ApiModelProperty("创建时交易哈希")
    var createTxHash: String? = null

    @ApiModelProperty("资产总量")
    var wiccAmount: String? = null

    @ApiModelProperty("债务总量")
    var wusdAmount: String? = null

    @ApiModelProperty("清算价格（$）")
    var clearingPrice: String? = null

    @ApiModelProperty("拥有者地址")
    var ownerAddress: String? = null

    @ApiModelProperty("链上创建时间")
    var initialedAt: Date? = null

    @ApiModelProperty("update时间")
    var updatedAt: Date? = null

    @ApiModelProperty("createdAt时间")
    var createdAt: Date? = null

    @ApiModelProperty("待清算资产量(WICC)")
    var assertValue: BigDecimal? = null

    @ApiModelProperty("待清算资产量($)")
    var assertValueDollar: BigDecimal? = null

    @ApiModelProperty("清算需要付出")
    var payValue: BigDecimal? = null

    @ApiModelProperty("折扣")
    var discount: String? = null

    @ApiModelProperty("稳定费")
    var fee: String? = null

}