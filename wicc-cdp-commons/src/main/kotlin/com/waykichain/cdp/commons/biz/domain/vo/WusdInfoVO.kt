package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class WusdInfoVO {
    @ApiModelProperty("current supply wusd")
    var currentSupply: BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wusd generated in 24 hours")
    var wusdGenerated:  String  = "0"

    @ApiModelProperty("wusd burned in 24 hours")
    var wusdBurned:  String = "0"
}