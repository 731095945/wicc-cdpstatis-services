package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class WiccListVO {
    var list = arrayListOf<WiccVO>()
}

class WiccVO {
    @ApiModelProperty("wicc当天Addition")
    var wiccAddtion: BigDecimal  = BigDecimal.ZERO

    @ApiModelProperty("wicc当天Redemption")
    var wiccRedemption:  BigDecimal  = BigDecimal.ZERO

    @ApiModelProperty("time")
    var time:  String? = null

    @ApiModelProperty("price")
    var price:  BigDecimal? = null

    @ApiModelProperty("wicc当天lock")
    var wiccTotalLock: BigDecimal  = BigDecimal.ZERO
}