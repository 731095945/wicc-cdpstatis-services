package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.commons.biz.domain.po.CdpLdTxListPO
import com.waykichain.cdp.commons.biz.domain.po.CdpTxListPO
import com.waykichain.cdp.entity.domain.CdpTx
import org.springframework.data.domain.Page

interface CdpTxService {
    fun getById(id:Long): CdpTx?

    fun save(cdpTx:CdpTx): CdpTx?

    fun queryByPageAndCdpIds(po: CdpTxListPO): Page<CdpTx>

    fun queryByPageAndLdAddr(po: CdpLdTxListPO): Page<CdpTx>

    fun findByHash(txid: String): CdpTx?

    fun getLastCdpOperateTime(cdpId: String): Long

    fun getMaxHeight(): Int
}
