package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class CdpStatisticsListVO {
    var list = arrayListOf<CdpStatisticsVO>()
}

open class CdpStatisticsVO {

    @ApiModelProperty("cdp当天增加数")
    var cdpAddition: Int = 0

    @ApiModelProperty("cdp当天关闭数")
    var cdpClose: Int = 0

    @ApiModelProperty("time")
    var time: String? = null

    @ApiModelProperty("price")
    var price:  BigDecimal? = null

    @ApiModelProperty("cdp total 增加数")
    var cdpTotalActive: Int = 0
}