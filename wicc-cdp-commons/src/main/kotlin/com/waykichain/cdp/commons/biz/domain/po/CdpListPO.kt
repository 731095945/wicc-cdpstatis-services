package com.waykichain.cdp.commons.biz.domain.po

import com.waykichain.chain.po.v2.BasePageablePO
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class CdpListPO :  BasePageablePO() {

    @ApiModelProperty("状态（100-open,200-正常，300-不正常，900-已关闭的，1000-全部）")
    var status: Int? = null

    @ApiModelProperty("wicc 价格",hidden = true)
    var price: BigDecimal? = null
}