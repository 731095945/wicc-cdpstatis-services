package com.waykichain.cdp.commons.biz.dict

enum class CdpStatisType (val code: Int,  val description: String) {
    WICC(0, "WICC"),
    WUSD(1,"WUSD"),
    WGRT(2,"WGRT"),
    CDP(3,"CDP")
}