package com.waykichain.cdp.commons.biz.service

import com.waykichain.cdp.entity.domain.DexTradeSettleTx

interface DexTradeSettleTxService {
    fun getById(id:Long): DexTradeSettleTx?

    fun save(dexTradeSettleTx:DexTradeSettleTx): DexTradeSettleTx
}
