package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class WgrtInfoVO {
    @ApiModelProperty("wgrt burn Amount in 24h")
    var wgrtBurnAmount: BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wgrt change rate in 24h")
    var wgrtChageRate:  BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wgrt total burned")
    var wgrtTotalBurned:  BigDecimal = BigDecimal.ZERO

    @ApiModelProperty("wgrt total issue")
    var wgrtTotalIssue: BigDecimal = BigDecimal(21000000000)

}