package com.waykichain.cdp.commons.biz.service.impl

import com.waykichain.cdp.commons.biz.domain.po.CdpLdTxListPO
import com.waykichain.cdp.commons.biz.domain.po.CdpTxListPO
import com.waykichain.cdp.commons.biz.service.CdpTxService
import com.waykichain.cdp.entity.domain.CdpTx
import com.waykichain.cdp.entity.domain.QCdpTx
import com.waykichain.cdp.repository.CdpTxRepository
import com.waykichain.chain.dict.TransactionType
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

@Service
class CdpTxServiceImpl: CdpTxService {

    override fun getById(id:Long): CdpTx? {
        return cdpTxRepository.findOne(id)
    }

    override fun save(cdpTx:CdpTx): CdpTx {
        return cdpTxRepository.saveAndFlush(cdpTx)
    }

    override fun queryByPageAndCdpIds(po: CdpTxListPO): Page<CdpTx> {

        var predicate = QCdpTx.cdpTx.cdpId.`in`(po.cdpIds)
        val sort = Sort(Sort.Direction.DESC, "blockTimestamp")
        val pageRequest = PageRequest(po.currentpage-1, po.pagesize, sort)
        return cdpTxRepository.findAll(predicate, pageRequest)
    }

    override fun queryByPageAndLdAddr(po: CdpLdTxListPO): Page<CdpTx> {

        var predicate = QCdpTx.cdpTx.ownerAddr.eq(po.address)
        predicate = predicate.and(QCdpTx.cdpTx.txType.eq(TransactionType.CDP_LIQUIDATE_TX.name))
        val sort = Sort(Sort.Direction.DESC, "updatedAt")
        val pageRequest = PageRequest(po.currentpage-1, po.pagesize, sort)
        return  cdpTxRepository.findAll(predicate, pageRequest)
    }


    override fun findByHash(txid: String): CdpTx? {
        return cdpTxRepository.findAll(QCdpTx.cdpTx.txid.eq(txid)).firstOrNull()
    }


    override fun getLastCdpOperateTime(cdpId: String): Long {

        return cdpTxRepository.queryLastTxId(cdpId)
    }

    override  fun getMaxHeight(): Int {
        return  cdpTxRepository.queryMaxHeight()
    }


    @Autowired lateinit var cdpTxRepository: CdpTxRepository

}
