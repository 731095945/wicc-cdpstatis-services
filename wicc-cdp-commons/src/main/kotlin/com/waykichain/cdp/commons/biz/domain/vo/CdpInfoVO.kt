package com.waykichain.cdp.commons.biz.domain.vo

import io.swagger.annotations.ApiModelProperty

class CdpInfoVO {
    @ApiModelProperty("global CR")
    var globalCR: String = "0"

    @ApiModelProperty("global active cdps")
    var globalCdp:  Int = 0

    @ApiModelProperty("addtional cdps in 24h")
    var addCdp:  Int = 0

    @ApiModelProperty("closed cdps in 24h")
    var closeCdp:  Int = 0
}