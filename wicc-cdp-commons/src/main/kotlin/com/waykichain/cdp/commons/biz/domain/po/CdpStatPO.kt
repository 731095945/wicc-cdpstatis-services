package com.waykichain.cdp.commons.biz.domain.po

import com.waykichain.cdp.commons.biz.util.DateTimeUtil
import io.swagger.annotations.ApiModelProperty

class CdpStatPO {

    @ApiModelProperty("开始时间：yyyy-MM-dd 单位：天",hidden = true)
    var startTime: String = "2018-09-01" //

    @ApiModelProperty("结束时间：yyyy-MM-dd 单位：天",hidden = true)
    var endTime: String  = DateTimeUtil.getToday()

    @ApiModelProperty("间隔时间:ONE_MONTH,ONE_WEEK,ALL")
    var timeIntervalType:String? = null

    @ApiModelProperty("type= WICC/WUSD/WGRT/CDP")
    var type:String ?= null

}

