package com.waykichain.cdp.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: yanjunlin
 * @CreateDate: 2019/7/31 11:47
 * @Description:
 */
public class MyBeanUtils {


    public static void copyProperties (Object source, Object target) {
        copyPropertiesDo(source, target, false);
    }

    /**
     * 只将target未赋值的数据进行赋值
     * @param source
     * @param target
     */
    public static void copyNotNullProperties (Object source, Object target) {
        copyPropertiesDo(source, target, true);
    }

    private static void copyPropertiesDo (Object source, Object target, Boolean isNotNull) {

        if (source == null || target == null) {
            return;
        }
        try {
            Map<String, Field> srcFieldMap= getAssignableFieldsMap(source);
            Map<String, Field> targetFieldMap = getAssignableFieldsMap(target);
            for (String srcFieldName : srcFieldMap.keySet()) {
                Field srcField = srcFieldMap.get(srcFieldName);
                if (srcField == null || srcField.get(source) == null) {
                    continue;
                }
                // 变量名需要相同
                if (!targetFieldMap.keySet().contains(srcFieldName)) {
                    continue;
                }
                Field targetField = targetFieldMap.get(srcFieldName);
                if (targetField == null) {
                    continue;
                }
                if (isNotNull) {
                    continue;
                }
                //BigDecimal to String
                if (srcField.getType() == BigDecimal.class && targetField.getType() == String.class) {
                    BigDecimal b = (BigDecimal) srcField.get(source);
                    targetField.set(target,b.stripTrailingZeros().toPlainString());
                    continue;
                }

                // 类型需要相同
                if (!srcField.getType().equals(targetField.getType())) {
                    continue;
                }

                targetField.set(target,srcField.get(source));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return ;
    }


    private static Map<String, Field> getAssignableFieldsMap(Object obj) {
        if (obj == null) {
            return new HashMap<>();
        }
        Map<String, Field> fieldMap = new HashMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            // 过滤不需要拷贝的属性
            if (Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers())) {
                continue;
            }
            field.setAccessible(true);
            fieldMap.put(field.getName(), field);
        }
        return fieldMap;
    }

}
