package com.waykichain.cdp.util;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;

public class StringUtil {

    public static final Long SYSTEM_SEND_COIN=85171907L;



    /**
     * 字符类型 Hex码转字符,  Convert Contract Content Hex to Contract Content String
     * @param data
     * @return
     */
    public static String ConvertContractData(String data)  {
        if (StringUtils.isBlank(data)) { return data; }
        byte[] utf8Data = new byte[data.length()/2];
        for(int i=0,j=0; i<data.length(); i+=2,j++) {
            String temp = data.substring(i, i+2);
            utf8Data[j] = (byte)Integer.parseInt(temp,16);
        }
        try {
            return  new String(utf8Data,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //    logger.error("转化lua脚本失败直接返回hex码");
        }
        return data;
    }


    //转换十六进制编码为字符串
    public static String hexToString(String s) {
        if ("0x".equals(s.substring(0, 2))) {
            s = s.substring(2);
        }
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(
                        i * 2, i * 2 + 2), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            s = new String(baKeyword, "utf-8");//UTF-16le:Not
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return s;
    }

    public static String upsidedownHex(String s){
        StringBuffer sb = new StringBuffer();
        for (int i = s.length(); i > 1; i = i - 2) {
            sb.append(s.substring(i - 2, i));
        }
        return sb.toString();
    }

    //转换十六进制编码为UTF8
    public static String hexToUTF8(String s) {
        if (s == null || s.equals("")) {
            return null;
        }

        try {
            int total = s.length() / 2;
            int pos = 0;

            byte[] buffer = new byte[total];
            for (int i = 0; i < total; i++) {
                int start = i * 2;

                buffer[i] = (byte) Integer.parseInt(s.substring(start, start + 2), 16);
                pos++;
            }

            return new String(buffer, 0, pos, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }


    public static String toHexString(String s) {
        StringBuilder sb = new StringBuilder();
        String s4 = null;
        int ch = 0;
        for (int i = 0; i < s.length(); i++) {
            ch = (int) s.charAt(i);
            s4 = Integer.toHexString(ch);
            sb.append(s4);
        }
        return sb.toString();//0x表示十六进制
    }

    public static String fillZero(String str, int maxlength) {
        int filllenth = maxlength - str.length();
        StringBuilder sb = new StringBuilder(str);
        for (int i = 0; i < filllenth; i++) {
            sb.append("0");
        }
        return sb.toString();
    }

    public static String utf8ToHexString(String s, int maxlength) {
        if (s == null || s.equals("")) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        try {
            char c;
            for (int i = 0; i < s.length(); i++) {
                c = s.charAt(i);
                byte[] b;

                b = Character.toString(c).getBytes("utf-8");

                for (int j = 0; j < b.length; j++) {
                    int k = b[j];
                    if (k < 0)
                        k += 256;
                    sb.append(Integer.toHexString(k));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        int gap = (maxlength * 2 - sb.length());
        if(gap > 0) {
            for(int j = 0; j < gap; j++) {
                sb.append("0");
            }
        }
        return sb.toString();
    }

/*


    public static void main(String[] args) {

        System.out.println("4 byte number >");
        String param = "9f860100";//99999
        System.out.println(param+"  :  "+Long.parseLong(StringUtil.upsidedownHex(param), 16));

        System.out.println("8 byte number >");
        String param2 = "d0a0b40400000000";//78946512
        System.out.println(param2+"  :  "+Long.parseLong(StringUtil.upsidedownHex(param2), 16));

        System.out.println("char >");
        String param3 = "3535736466734144";//55sdfsAD
        System.out.println(param3+"  :  "+StringUtil.hexToString(param3));

        System.out.println("UTF-8 >");
        String param4 = "e6b58be8af95e4b8ade69687e698af";//测试中文是
        System.out.println(param4+"  :  "+StringUtil.hexToString(param4));
        System.out.println(param4+"  :  "+StringUtil.hexToUTF8(param4));

        System.out.println("hex >");
    }
*/

}


