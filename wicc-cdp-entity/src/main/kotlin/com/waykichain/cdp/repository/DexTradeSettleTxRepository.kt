package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.DexTradeSettleTx
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface DexTradeSettleTxRepository : JpaRepository<DexTradeSettleTx, Long>,
    QueryDslPredicateExecutor<DexTradeSettleTx>
