package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.CdpStats
import org.apache.ibatis.annotations.Update
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.util.*

interface CdpStatsRepository : JpaRepository<CdpStats, Long>,
    QueryDslPredicateExecutor<CdpStats> {

    @Query("select ifnull(sum(median_price),0) FROM median_price where date(created_at) = ?1 and coin_symbol = ?2", nativeQuery = true)
    fun getSumPrice(statTime:String, coinSymbol:String):BigDecimal

    @Query("select count(0) FROM median_price where date(created_at) = ?1 and coin_symbol = ?2", nativeQuery = true)
    fun getCountPrice(statTime:String, coinSymbol:String):Integer

    @Query("select * from cdp_stats where date(stats_time)>= ?1 and date(stats_time) <= ?2", nativeQuery = true)
    fun getCdpStats(startTime: String, endTime: String): List<CdpStats>

    @Query("select date(max(stats_time)) from cdp_stats", nativeQuery = true)
    fun getMaxCdpStatsDay(): Date

    @Query("select date(FROM_UNIXTIME(min(block_timestamp)/1000)) from cdp_tx", nativeQuery = true)
    fun getMinCdpTxDay(): Date

    @Query("select ifnull(sum(wgrt_issue_amount),0) from price_median_wgrt_tx where  date(FROM_UNIXTIME(block_timestamp)) = ?1",nativeQuery = true)
    fun getWgrtIssueAmount(day: String): BigDecimal

    @Query("select ifnull(sum(scoin_burn_amount),0) from price_median_wgrt_tx where date(FROM_UNIXTIME(block_timestamp)) = ?1", nativeQuery = true)
    fun getWusdBurnFromForceLiquate(day: String):BigDecimal

    @Query("select ifnull(sum(asset_amount),0) from dex_trade_settle_tx where date(FROM_UNIXTIME(confirmed_time)) = ?1", nativeQuery = true)
    fun getWgrtBurnAmount(day: String):BigDecimal

    @Query("select ifnull(sum(scoin_amount),0) from cdp_tx where tx_type = 'CDP_STAKE_TX' and date(FROM_UNIXTIME(block_timestamp/1000)) = ?1 ", nativeQuery = true)
    fun getWusdGenerated(date: String): BigDecimal //21

    @Modifying
    @Transactional
    @Query("set time_zone = '+8:00';",  nativeQuery = true)
    fun setTimeZone()

    @Modifying
    @Transactional
    @Query("set GLOBAL time_zone = '+8:00';",  nativeQuery = true)
    fun setGlobalTimeZone()

    @Modifying
    @Transactional
    @Query("set time_zone = '-8:00';",  nativeQuery = true)
    fun clearTimeZone()

    @Query("select ifnull(sum(scoin_burn_amount),0) from cdp_tx where (tx_type = 'CDP_LIQUIDATE_TX' or tx_type = 'CDP_REDEEM_TX') and date(FROM_UNIXTIME(block_timestamp/1000)) = ?1", nativeQuery = true)
    fun getWusdBurned(day: String): BigDecimal//22,23

    @Query("select ifnull(sum(bcoin_amount),0) from cdp_tx where tx_type = 'CDP_STAKE_TX' and \n" +
            "date(FROM_UNIXTIME(block_timestamp/1000)) = ?1",nativeQuery = true)
    fun getWiccAddtion(day: String): BigDecimal//21

    @Query("select ifnull(sum(bcoin_amount),0) from cdp_tx where tx_type = 'CDP_STAKE_TX' and \n" +
            " block_timestamp/1000 <=(UNIX_TIMESTAMP(?1) + 24*60*60) ",nativeQuery = true)
    fun getWiccAddtionByDay(day:String):BigDecimal

    @Query("select ifnull(sum(bcoin_redeem_amount),0) from cdp_tx where (tx_type = 'CDP_LIQUIDATE_TX' or tx_type = 'CDP_REDEEM_TX') and date(FROM_UNIXTIME(block_timestamp/1000)) = ?1", nativeQuery = true)
    fun getWiccRedemption(day: String): BigDecimal//22,23

    @Query("select ifnull(sum(bcoin_redeem_amount),0) from cdp_tx where (tx_type = 'CDP_LIQUIDATE_TX' or tx_type = 'CDP_REDEEM_TX') and  block_timestamp/1000 <=(UNIX_TIMESTAMP(?1) + 24*60*60) ", nativeQuery = true)
    fun getWiccRedemptionByDay(day: String): BigDecimal

    @Query("select ifnull(sum(bcoin_redeem_amount),0) from price_median_wgrt_tx where date(FROM_UNIXTIME(block_timestamp)) = ?1", nativeQuery = true)
    fun getWiccRedemptionFromForLiquate(day:String): BigDecimal

    @Query("select ifnull(sum(bcoin_redeem_amount),0) from price_median_wgrt_tx where block_timestamp <=(UNIX_TIMESTAMP(?1) + 24*60*60) ", nativeQuery = true)
    fun getWiccRedemptionFromForLiquateByDay(day:String):BigDecimal

    @Query(" select ifnull(sum(bcoin_redeem_amount),0) from price_median_wgrt_tx where  block_timestamp >= UNIX_TIMESTAMP(NOW() - interval 24 hour) " ,nativeQuery = true)
    fun getWiccRedemptionBefore24H():BigDecimal

    @Query("select count(0) from cdp_tx where from_status = 0 and to_status = 100 and date(FROM_UNIXTIME(block_timestamp/1000)) = ?1", nativeQuery = true)
    fun getCdpAdded(day: String): Int//100

    @Query("select count(0) from cdp_tx where from_status = 0 and to_status = 100 and block_timestamp/1000 <=(UNIX_TIMESTAMP(?1) + 24*60*60)", nativeQuery = true)
    fun getCdpAddedByDay(day: String):Int

    @Query("select count(0) from cdp_tx where to_status = 900 and date(FROM_UNIXTIME(block_timestamp/1000)) = ?1", nativeQuery = true)
    fun getCdpClosed(day: String): Int//900

    @Query("select count(0) from cdp_tx where to_status = 900 and block_timestamp/1000 <=(UNIX_TIMESTAMP(?1) + 24*60*60) ", nativeQuery = true)
    fun getCdpClosedByDay(day: String): Int//900


    @Query("select ifnull(sum(scoin_amount),0) from cdp where status = 100", nativeQuery = true)
    fun getCurrentSupply(): BigDecimal //100

    @Query("select ifnull(sum(scoin_amount),0) from cdp_tx where tx_type = 'CDP_STAKE_TX' and block_timestamp/1000>=UNIX_TIMESTAMP(NOW() - interval 24 hour)", nativeQuery = true)
    fun getGeneratedWUSD(): BigDecimal //21

    @Query("select ifnull(sum(scoin_amount),0) from cdp_tx where tx_type = 'CDP_STAKE_TX' and block_timestamp/1000 <= (UNIX_TIMESTAMP(?1) + 24*60*60)", nativeQuery = true)
    fun getGenerateWusdByDay(day:String):BigDecimal

    @Query("select ifnull(sum(scoin_burn_amount),0) from cdp_tx where (tx_type ='CDP_REDEEM_TX' or tx_type ='CDP_LIQUIDATE_TX') and block_timestamp/1000>=UNIX_TIMESTAMP(NOW() - interval 24 hour)", nativeQuery = true)
    fun getBurnedWUSD(): BigDecimal // 22,23

    @Query("select ifnull(sum(scoin_burn_amount),0) from cdp_tx where (tx_type ='CDP_REDEEM_TX' or tx_type ='CDP_LIQUIDATE_TX') and block_timestamp/1000 <= (UNIX_TIMESTAMP(?1) + 24*60*60)", nativeQuery = true)
    fun getBurnedWusdByDay(day:String):BigDecimal

    @Query("select ifnull(sum(scoin_burn_amount),0) from price_median_wgrt_tx where block_timestamp >= UNIX_TIMESTAMP(NOW() - interval 24 hour)", nativeQuery = true)
    fun getBurnWusdFromForceLiquate():BigDecimal

    @Query("select ifnull(sum(scoin_burn_amount),0) from price_median_wgrt_tx where block_timestamp <=(UNIX_TIMESTAMP(?1) + 24*60*60)", nativeQuery = true)
    fun getBurnWusdFromForceLiquateByDay(day:String):BigDecimal

    @Query("select ifnull(sum(bcoin_amount),0) from cdp where status = 100", nativeQuery = true)
    fun getLockWICC(): BigDecimal //100

    @Query("select ifnull(sum(bcoin_amount),0) from cdp_tx where tx_type = 'CDP_STAKE_TX' and block_timestamp/1000 >= UNIX_TIMESTAMP(NOW() - interval 24 hour)", nativeQuery = true)
    fun getAddWICC(): BigDecimal //21

    @Query("select ifnull(sum(bcoin_redeem_amount),0) from cdp_tx where (tx_type = 'CDP_REDEEM_TX' or tx_type = 'CDP_LIQUIDATE_TX') and block_timestamp/1000>=UNIX_TIMESTAMP(NOW() - interval 24 hour)", nativeQuery = true)
    fun getRedemptWICC(): BigDecimal

    @Query("select count(0) from cdp where status = 100", nativeQuery = true)
    fun getActiveCDP(): Int

    @Query("select count(*) from cdp where  cdp_created_at >= NOW() - interval 24 hour ", nativeQuery = true)
    fun getAddCDP(): Int // 100

    @Query("select count(*) from cdp where  status = 900 and updated_at >= NOW() - interval 24 hour ", nativeQuery = true)
    fun getCloseCDP(): Int

    @Query("select ifnull(sum(asset_amount),0) from dex_trade_settle_tx where confirmed_time >= UNIX_TIMESTAMP(NOW() - interval 24 hour)", nativeQuery = true)
    fun getWgrtBurn(): BigDecimal

    @Query("select ifnull(sum(asset_amount),0) from dex_trade_settle_tx ", nativeQuery = true)
    fun getTotalWgrtBurn(): BigDecimal

    @Query("select median_price from median_price where coin_symbol = 'WGRT' ORDER BY id DESC limit 0,1", nativeQuery = true)
    fun getWgrtPrice(): BigDecimal

    @Query("select median_price from median_price where coin_symbol = 'WGRT' and created_at > (Now() - interval 24 hour)\n" +
            "ORDER BY id ASC limit 0,1", nativeQuery = true)
    fun getWgrtPriceBefore24H(): BigDecimal

    @Query("select ifnull(sum(wgrt_issue_amount) - sum(wgrt_burn_amount),0)  FROM cdp_stats ", nativeQuery = true)
    fun getWgrtTotal():BigDecimal
}
