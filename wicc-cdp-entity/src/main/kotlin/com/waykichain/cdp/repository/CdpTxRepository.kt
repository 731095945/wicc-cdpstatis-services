package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.CdpTx
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface CdpTxRepository : JpaRepository<CdpTx, Long>,
    QueryDslPredicateExecutor<CdpTx> {

    @Query(" select ifnull(max(block_timestamp),0) from cdp_tx where cdp_id =  ?1", nativeQuery = true)
    fun queryLastTxId(cdpId: String): Long

    @Query("select ifnull(max(block_height),0) from cdp_tx", nativeQuery = true)
    fun queryMaxHeight(): Int
}
