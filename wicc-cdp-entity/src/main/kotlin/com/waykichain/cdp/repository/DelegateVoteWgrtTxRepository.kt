package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.DelegateVoteWgrtTx
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor
import java.math.BigDecimal

interface DelegateVoteWgrtTxRepository : JpaRepository<DelegateVoteWgrtTx, Long>,
    QueryDslPredicateExecutor<DelegateVoteWgrtTx> {

    @Query("select ifnull(sum(wgrt_reward),0) from delegate_vote_wgrt_tx where date(FROM_UNIXTIME(confirmed_time)) = ?1 and confirmed_time > 1666750273",nativeQuery = true)
    fun getWgrtIssueAmount(day: String): BigDecimal


    @Query("select ifnull(sum(wgrt_reward),0) from delegate_vote_wgrt_tx where confirmed_time > UNIX_TIMESTAMP(Now() - interval 24 hour)" , nativeQuery = true)
    fun getWgrtAmountBefore24H(): BigDecimal

}
