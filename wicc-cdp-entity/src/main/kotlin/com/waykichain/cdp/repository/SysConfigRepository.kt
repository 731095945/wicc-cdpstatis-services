package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.SysConfig
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface SysConfigRepository : JpaRepository<SysConfig, Long>,
    QueryDslPredicateExecutor<SysConfig>
