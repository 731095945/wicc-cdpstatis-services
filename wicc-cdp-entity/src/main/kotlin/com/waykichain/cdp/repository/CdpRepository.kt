package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.Cdp
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor
import java.math.BigDecimal

interface CdpRepository : JpaRepository<Cdp, Long>,
    QueryDslPredicateExecutor<Cdp> {

    @Query("update cdp set status =900 where force_liquidate_price >= ?1 ", nativeQuery = true)
    fun updateCdpStatus(price:BigDecimal)

    @Query("select * from cdp where force_liquidate_price >= ?1 ", nativeQuery = true)
    fun queryUnsafeCdpList(price:BigDecimal) : List<Cdp>

}
