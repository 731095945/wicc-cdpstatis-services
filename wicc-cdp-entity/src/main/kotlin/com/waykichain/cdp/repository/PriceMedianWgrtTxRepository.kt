package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.PriceMedianWgrtTx
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface PriceMedianWgrtTxRepository : JpaRepository<PriceMedianWgrtTx, Long>,
    QueryDslPredicateExecutor<PriceMedianWgrtTx>
