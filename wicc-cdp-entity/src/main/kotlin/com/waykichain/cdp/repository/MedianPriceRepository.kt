package com.waykichain.cdp.repository

import com.waykichain.cdp.entity.domain.MedianPrice
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MedianPriceRepository : JpaRepository<MedianPrice, Long>,
    QueryDslPredicateExecutor<MedianPrice>
