package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * PriceMedianTx is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class PriceMedianTx implements Serializable {

    @Column("block_height")
    private Integer blockHeight;

    @Column("block_timestamp")
    private Long blockTimestamp;

    @Id
    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("receipt")
    private String receipt;

    @Column("txid")
    private String txid;

    @Column("tx_type")
    private String txType;

    @Column("wgrt_issue_amount")
    private java.math.BigDecimal wgrtIssueAmount;

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(Integer blockHeight) {
        this.blockHeight = blockHeight;
    }

    public Long getBlockTimestamp() {
        return blockTimestamp;
    }

    public void setBlockTimestamp(Long blockTimestamp) {
        this.blockTimestamp = blockTimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public java.math.BigDecimal getWgrtIssueAmount() {
        return wgrtIssueAmount;
    }

    public void setWgrtIssueAmount(java.math.BigDecimal wgrtIssueAmount) {
        this.wgrtIssueAmount = wgrtIssueAmount;
    }

}

