package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDexTradeSettleTx is a Querydsl query type for DexTradeSettleTx
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QDexTradeSettleTx extends com.querydsl.sql.RelationalPathBase<DexTradeSettleTx> {

    private static final long serialVersionUID = -1904324441;

    public static final QDexTradeSettleTx dexTradeSettleTx = new QDexTradeSettleTx("dex_trade_settle_tx");

    public final NumberPath<Long> assetAmount = createNumber("assetAmount", Long.class);

    public final StringPath blockHash = createString("blockHash");

    public final StringPath buyOrderId = createString("buyOrderId");

    public final NumberPath<Long> coinAmount = createNumber("coinAmount", Long.class);

    public final NumberPath<Long> confirmedTime = createNumber("confirmedTime", Long.class);

    public final DateTimePath<java.util.Date> createAt = createDateTime("createAt", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath txid = createString("txid");

    public final StringPath txType = createString("txType");

    public final DateTimePath<java.util.Date> updateAt = createDateTime("updateAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<DexTradeSettleTx> primary = createPrimaryKey(id);

    public QDexTradeSettleTx(String variable) {
        super(DexTradeSettleTx.class, forVariable(variable), "null", "dex_trade_settle_tx");
        addMetadata();
    }

    public QDexTradeSettleTx(String variable, String schema, String table) {
        super(DexTradeSettleTx.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDexTradeSettleTx(String variable, String schema) {
        super(DexTradeSettleTx.class, forVariable(variable), schema, "dex_trade_settle_tx");
        addMetadata();
    }

    public QDexTradeSettleTx(Path<? extends DexTradeSettleTx> path) {
        super(path.getType(), path.getMetadata(), "null", "dex_trade_settle_tx");
        addMetadata();
    }

    public QDexTradeSettleTx(PathMetadata metadata) {
        super(DexTradeSettleTx.class, metadata, "null", "dex_trade_settle_tx");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(assetAmount, ColumnMetadata.named("asset_amount").withIndex(7).ofType(Types.BIGINT).withSize(19));
        addMetadata(blockHash, ColumnMetadata.named("block_hash").withIndex(5).ofType(Types.VARCHAR).withSize(128));
        addMetadata(buyOrderId, ColumnMetadata.named("buy_order_id").withIndex(3).ofType(Types.VARCHAR).withSize(128));
        addMetadata(coinAmount, ColumnMetadata.named("coin_amount").withIndex(6).ofType(Types.BIGINT).withSize(19));
        addMetadata(confirmedTime, ColumnMetadata.named("confirmed_time").withIndex(4).ofType(Types.BIGINT).withSize(19));
        addMetadata(createAt, ColumnMetadata.named("create_at").withIndex(9).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(19).notNull());
        addMetadata(txid, ColumnMetadata.named("txid").withIndex(2).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txType, ColumnMetadata.named("tx_type").withIndex(8).ofType(Types.VARCHAR).withSize(128));
        addMetadata(updateAt, ColumnMetadata.named("update_at").withIndex(10).ofType(Types.TIMESTAMP).withSize(19));
    }

}

