package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCdpStats is a Querydsl query type for CdpStats
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QCdpStats extends com.querydsl.sql.RelationalPathBase<CdpStats> {

    private static final long serialVersionUID = -1789037189;

    public static final QCdpStats cdpStats = new QCdpStats("cdp_stats");

    public final NumberPath<java.math.BigDecimal> bcoinCollateralAmount = createNumber("bcoinCollateralAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> bcoinRedeemAmount = createNumber("bcoinRedeemAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> cdpCloseCount = createNumber("cdpCloseCount", Integer.class);

    public final NumberPath<Integer> cdpCreateCount = createNumber("cdpCreateCount", Integer.class);

    public final NumberPath<Integer> cdpTotalActive = createNumber("cdpTotalActive", Integer.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> scoinBurnAmount = createNumber("scoinBurnAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> scoinMintAmount = createNumber("scoinMintAmount", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> statsTime = createDateTime("statsTime", java.util.Date.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> wgrtBurnAmount = createNumber("wgrtBurnAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> wgrtIssueAmount = createNumber("wgrtIssueAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> wgrtPrice = createNumber("wgrtPrice", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> wiccPrice = createNumber("wiccPrice", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> wiccTotalLock = createNumber("wiccTotalLock", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> wusdTotalSupply = createNumber("wusdTotalSupply", java.math.BigDecimal.class);

    public final com.querydsl.sql.PrimaryKey<CdpStats> primary = createPrimaryKey(id);

    public QCdpStats(String variable) {
        super(CdpStats.class, forVariable(variable), "null", "cdp_stats");
        addMetadata();
    }

    public QCdpStats(String variable, String schema, String table) {
        super(CdpStats.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCdpStats(String variable, String schema) {
        super(CdpStats.class, forVariable(variable), schema, "cdp_stats");
        addMetadata();
    }

    public QCdpStats(Path<? extends CdpStats> path) {
        super(path.getType(), path.getMetadata(), "null", "cdp_stats");
        addMetadata();
    }

    public QCdpStats(PathMetadata metadata) {
        super(CdpStats.class, metadata, "null", "cdp_stats");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bcoinCollateralAmount, ColumnMetadata.named("bcoin_collateral_amount").withIndex(2).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(bcoinRedeemAmount, ColumnMetadata.named("bcoin_redeem_amount").withIndex(5).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(cdpCloseCount, ColumnMetadata.named("cdp_close_count").withIndex(7).ofType(Types.INTEGER).withSize(10));
        addMetadata(cdpCreateCount, ColumnMetadata.named("cdp_create_count").withIndex(6).ofType(Types.INTEGER).withSize(10));
        addMetadata(cdpTotalActive, ColumnMetadata.named("cdp_total_active").withIndex(17).ofType(Types.INTEGER).withSize(10));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(9).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(19).notNull());
        addMetadata(scoinBurnAmount, ColumnMetadata.named("scoin_burn_amount").withIndex(4).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(scoinMintAmount, ColumnMetadata.named("scoin_mint_amount").withIndex(3).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(statsTime, ColumnMetadata.named("stats_time").withIndex(8).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(10).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(wgrtBurnAmount, ColumnMetadata.named("wgrt_burn_amount").withIndex(13).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(wgrtIssueAmount, ColumnMetadata.named("wgrt_issue_amount").withIndex(12).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(wgrtPrice, ColumnMetadata.named("wgrt_price").withIndex(11).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(wiccPrice, ColumnMetadata.named("wicc_price").withIndex(14).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(wiccTotalLock, ColumnMetadata.named("wicc_total_lock").withIndex(16).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(wusdTotalSupply, ColumnMetadata.named("wusd_total_supply").withIndex(15).ofType(Types.DECIMAL).withSize(32).withDigits(8));
    }

}

