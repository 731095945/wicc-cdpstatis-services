package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Cdp is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class Cdp implements Serializable {

    @Column("bcoin_amount")
    private java.math.BigDecimal bcoinAmount;

    @Column("cdp_created_at")
    private java.util.Date cdpCreatedAt;

    @Column("cdp_id")
    private String cdpId;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("force_liquidate_price")
    private java.math.BigDecimal forceLiquidatePrice;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("liquidate_price")
    private java.math.BigDecimal liquidatePrice;

    @Column("owner_addr")
    private String ownerAddr;

    @Column("scoin_amount")
    private java.math.BigDecimal scoinAmount;

    @Column("status")
    private Integer status;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public java.math.BigDecimal getBcoinAmount() {
        return bcoinAmount;
    }

    public void setBcoinAmount(java.math.BigDecimal bcoinAmount) {
        this.bcoinAmount = bcoinAmount;
    }

    public java.util.Date getCdpCreatedAt() {
        return cdpCreatedAt;
    }

    public void setCdpCreatedAt(java.util.Date cdpCreatedAt) {
        this.cdpCreatedAt = cdpCreatedAt;
    }

    public String getCdpId() {
        return cdpId;
    }

    public void setCdpId(String cdpId) {
        this.cdpId = cdpId;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public java.math.BigDecimal getForceLiquidatePrice() {
        return forceLiquidatePrice;
    }

    public void setForceLiquidatePrice(java.math.BigDecimal forceLiquidatePrice) {
        this.forceLiquidatePrice = forceLiquidatePrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getLiquidatePrice() {
        return liquidatePrice;
    }

    public void setLiquidatePrice(java.math.BigDecimal liquidatePrice) {
        this.liquidatePrice = liquidatePrice;
    }

    public String getOwnerAddr() {
        return ownerAddr;
    }

    public void setOwnerAddr(String ownerAddr) {
        this.ownerAddr = ownerAddr;
    }

    public java.math.BigDecimal getScoinAmount() {
        return scoinAmount;
    }

    public void setScoinAmount(java.math.BigDecimal scoinAmount) {
        this.scoinAmount = scoinAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

