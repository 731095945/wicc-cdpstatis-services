package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCdpTx is a Querydsl query type for CdpTx
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QCdpTx extends com.querydsl.sql.RelationalPathBase<CdpTx> {

    private static final long serialVersionUID = -955618520;

    public static final QCdpTx cdpTx = new QCdpTx("cdp_tx");

    public final NumberPath<java.math.BigDecimal> bcoinAmount = createNumber("bcoinAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> bcoinRedeemAmount = createNumber("bcoinRedeemAmount", java.math.BigDecimal.class);

    public final StringPath bcoinSymbol = createString("bcoinSymbol");

    public final NumberPath<Integer> blockHeight = createNumber("blockHeight", Integer.class);

    public final NumberPath<Long> blockTimestamp = createNumber("blockTimestamp", Long.class);

    public final StringPath cdpCreatedAt = createString("cdpCreatedAt");

    public final StringPath cdpId = createString("cdpId");

    public final NumberPath<java.math.BigDecimal> collateralRatio = createNumber("collateralRatio", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> fees = createNumber("fees", java.math.BigDecimal.class);

    public final StringPath feeSymbol = createString("feeSymbol");

    public final NumberPath<Integer> fromStatus = createNumber("fromStatus", Integer.class);

    public final NumberPath<java.math.BigDecimal> fromWiccAmount = createNumber("fromWiccAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> fromWusdAmount = createNumber("fromWusdAmount", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath ownerAddr = createString("ownerAddr");

    public final StringPath ownerUid = createString("ownerUid");

    public final StringPath receipt = createString("receipt");

    public final NumberPath<java.math.BigDecimal> scoinAmount = createNumber("scoinAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> scoinBurnAmount = createNumber("scoinBurnAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> scoinInterest = createNumber("scoinInterest", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> scoinPenalty = createNumber("scoinPenalty", java.math.BigDecimal.class);

    public final NumberPath<Integer> toStatus = createNumber("toStatus", Integer.class);

    public final NumberPath<java.math.BigDecimal> toWiccAmount = createNumber("toWiccAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> toWusdAmount = createNumber("toWusdAmount", java.math.BigDecimal.class);

    public final StringPath txid = createString("txid");

    public final StringPath txType = createString("txType");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<CdpTx> primary = createPrimaryKey(id);

    public QCdpTx(String variable) {
        super(CdpTx.class, forVariable(variable), "null", "cdp_tx");
        addMetadata();
    }

    public QCdpTx(String variable, String schema, String table) {
        super(CdpTx.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCdpTx(String variable, String schema) {
        super(CdpTx.class, forVariable(variable), schema, "cdp_tx");
        addMetadata();
    }

    public QCdpTx(Path<? extends CdpTx> path) {
        super(path.getType(), path.getMetadata(), "null", "cdp_tx");
        addMetadata();
    }

    public QCdpTx(PathMetadata metadata) {
        super(CdpTx.class, metadata, "null", "cdp_tx");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bcoinAmount, ColumnMetadata.named("bcoin_amount").withIndex(12).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(bcoinRedeemAmount, ColumnMetadata.named("bcoin_redeem_amount").withIndex(28).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(bcoinSymbol, ColumnMetadata.named("bcoin_symbol").withIndex(26).ofType(Types.VARCHAR).withSize(255));
        addMetadata(blockHeight, ColumnMetadata.named("block_height").withIndex(4).ofType(Types.INTEGER).withSize(10));
        addMetadata(blockTimestamp, ColumnMetadata.named("block_timestamp").withIndex(5).ofType(Types.BIGINT).withSize(19));
        addMetadata(cdpCreatedAt, ColumnMetadata.named("cdp_created_at").withIndex(25).ofType(Types.VARCHAR).withSize(255));
        addMetadata(cdpId, ColumnMetadata.named("cdp_id").withIndex(6).ofType(Types.VARCHAR).withSize(128));
        addMetadata(collateralRatio, ColumnMetadata.named("collateral_ratio").withIndex(10).ofType(Types.DECIMAL).withSize(20).withDigits(2));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(23).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(fees, ColumnMetadata.named("fees").withIndex(14).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(feeSymbol, ColumnMetadata.named("fee_symbol").withIndex(15).ofType(Types.VARCHAR).withSize(255));
        addMetadata(fromStatus, ColumnMetadata.named("from_status").withIndex(18).ofType(Types.INTEGER).withSize(10));
        addMetadata(fromWiccAmount, ColumnMetadata.named("from_wicc_amount").withIndex(16).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(fromWusdAmount, ColumnMetadata.named("from_wusd_amount").withIndex(17).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(ownerAddr, ColumnMetadata.named("owner_addr").withIndex(7).ofType(Types.VARCHAR).withSize(128));
        addMetadata(ownerUid, ColumnMetadata.named("owner_uid").withIndex(8).ofType(Types.VARCHAR).withSize(128));
        addMetadata(receipt, ColumnMetadata.named("receipt").withIndex(22).ofType(Types.LONGVARCHAR).withSize(65535));
        addMetadata(scoinAmount, ColumnMetadata.named("scoin_amount").withIndex(11).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(scoinBurnAmount, ColumnMetadata.named("scoin_burn_amount").withIndex(27).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(scoinInterest, ColumnMetadata.named("scoin_interest").withIndex(9).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(scoinPenalty, ColumnMetadata.named("scoin_penalty").withIndex(13).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(toStatus, ColumnMetadata.named("to_status").withIndex(21).ofType(Types.INTEGER).withSize(10));
        addMetadata(toWiccAmount, ColumnMetadata.named("to_wicc_amount").withIndex(19).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(toWusdAmount, ColumnMetadata.named("to_wusd_amount").withIndex(20).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(txid, ColumnMetadata.named("txid").withIndex(2).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txType, ColumnMetadata.named("tx_type").withIndex(3).ofType(Types.VARCHAR).withSize(128));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(24).ofType(Types.TIMESTAMP).withSize(19));
    }

}

