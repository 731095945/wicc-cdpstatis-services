package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMedianPrice is a Querydsl query type for MedianPrice
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMedianPrice extends com.querydsl.sql.RelationalPathBase<MedianPrice> {

    private static final long serialVersionUID = -1385834828;

    public static final QMedianPrice medianPrice1 = new QMedianPrice("median_price");

    public final NumberPath<Integer> blockHeight = createNumber("blockHeight", Integer.class);

    public final StringPath coinSymbol = createString("coinSymbol");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath currencySymbol = createString("currencySymbol");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> medianPrice = createNumber("medianPrice", java.math.BigDecimal.class);

    public final com.querydsl.sql.PrimaryKey<MedianPrice> primary = createPrimaryKey(id);

    public QMedianPrice(String variable) {
        super(MedianPrice.class, forVariable(variable), "null", "median_price");
        addMetadata();
    }

    public QMedianPrice(String variable, String schema, String table) {
        super(MedianPrice.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMedianPrice(String variable, String schema) {
        super(MedianPrice.class, forVariable(variable), schema, "median_price");
        addMetadata();
    }

    public QMedianPrice(Path<? extends MedianPrice> path) {
        super(path.getType(), path.getMetadata(), "null", "median_price");
        addMetadata();
    }

    public QMedianPrice(PathMetadata metadata) {
        super(MedianPrice.class, metadata, "null", "median_price");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(blockHeight, ColumnMetadata.named("block_height").withIndex(5).ofType(Types.INTEGER).withSize(10));
        addMetadata(coinSymbol, ColumnMetadata.named("coin_symbol").withIndex(2).ofType(Types.VARCHAR).withSize(32));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(6).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(currencySymbol, ColumnMetadata.named("currency_symbol").withIndex(3).ofType(Types.VARCHAR).withSize(32));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(medianPrice, ColumnMetadata.named("median_price").withIndex(4).ofType(Types.DECIMAL).withSize(20).withDigits(8));
    }

}

