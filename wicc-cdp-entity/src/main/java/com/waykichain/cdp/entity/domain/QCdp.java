package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QCdp is a Querydsl query type for Cdp
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QCdp extends com.querydsl.sql.RelationalPathBase<Cdp> {

    private static final long serialVersionUID = 544256388;

    public static final QCdp cdp = new QCdp("cdp");

    public final NumberPath<java.math.BigDecimal> bcoinAmount = createNumber("bcoinAmount", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> cdpCreatedAt = createDateTime("cdpCreatedAt", java.util.Date.class);

    public final StringPath cdpId = createString("cdpId");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> forceLiquidatePrice = createNumber("forceLiquidatePrice", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> liquidatePrice = createNumber("liquidatePrice", java.math.BigDecimal.class);

    public final StringPath ownerAddr = createString("ownerAddr");

    public final NumberPath<java.math.BigDecimal> scoinAmount = createNumber("scoinAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<Cdp> primary = createPrimaryKey(id);

    public QCdp(String variable) {
        super(Cdp.class, forVariable(variable), "null", "cdp");
        addMetadata();
    }

    public QCdp(String variable, String schema, String table) {
        super(Cdp.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QCdp(String variable, String schema) {
        super(Cdp.class, forVariable(variable), schema, "cdp");
        addMetadata();
    }

    public QCdp(Path<? extends Cdp> path) {
        super(path.getType(), path.getMetadata(), "null", "cdp");
        addMetadata();
    }

    public QCdp(PathMetadata metadata) {
        super(Cdp.class, metadata, "null", "cdp");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bcoinAmount, ColumnMetadata.named("bcoin_amount").withIndex(4).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(cdpCreatedAt, ColumnMetadata.named("cdp_created_at").withIndex(8).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(cdpId, ColumnMetadata.named("cdp_id").withIndex(2).ofType(Types.VARCHAR).withSize(128));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(10).ofType(Types.TIMESTAMP).withSize(19).notNull());
        addMetadata(forceLiquidatePrice, ColumnMetadata.named("force_liquidate_price").withIndex(9).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(liquidatePrice, ColumnMetadata.named("liquidate_price").withIndex(6).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(ownerAddr, ColumnMetadata.named("owner_addr").withIndex(7).ofType(Types.VARCHAR).withSize(128));
        addMetadata(scoinAmount, ColumnMetadata.named("scoin_amount").withIndex(5).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(status, ColumnMetadata.named("status").withIndex(3).ofType(Types.INTEGER).withSize(10));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(11).ofType(Types.TIMESTAMP).withSize(19));
    }

}

