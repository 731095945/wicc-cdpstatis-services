package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * CdpStats is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class CdpStats implements Serializable {

    @Column("bcoin_collateral_amount")
    private java.math.BigDecimal bcoinCollateralAmount;

    @Column("bcoin_redeem_amount")
    private java.math.BigDecimal bcoinRedeemAmount;

    @Column("cdp_close_count")
    private Integer cdpCloseCount;

    @Column("cdp_create_count")
    private Integer cdpCreateCount;

    @Column("cdp_total_active")
    private Integer cdpTotalActive;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("scoin_burn_amount")
    private java.math.BigDecimal scoinBurnAmount;

    @Column("scoin_mint_amount")
    private java.math.BigDecimal scoinMintAmount;

    @Column("stats_time")
    private java.util.Date statsTime;

    @Column("updated_at")
    private java.util.Date updatedAt;

    @Column("wgrt_burn_amount")
    private java.math.BigDecimal wgrtBurnAmount;

    @Column("wgrt_issue_amount")
    private java.math.BigDecimal wgrtIssueAmount;

    @Column("wgrt_price")
    private java.math.BigDecimal wgrtPrice;

    @Column("wicc_price")
    private java.math.BigDecimal wiccPrice;

    @Column("wicc_total_lock")
    private java.math.BigDecimal wiccTotalLock;

    @Column("wusd_total_supply")
    private java.math.BigDecimal wusdTotalSupply;

    public java.math.BigDecimal getBcoinCollateralAmount() {
        return bcoinCollateralAmount;
    }

    public void setBcoinCollateralAmount(java.math.BigDecimal bcoinCollateralAmount) {
        this.bcoinCollateralAmount = bcoinCollateralAmount;
    }

    public java.math.BigDecimal getBcoinRedeemAmount() {
        return bcoinRedeemAmount;
    }

    public void setBcoinRedeemAmount(java.math.BigDecimal bcoinRedeemAmount) {
        this.bcoinRedeemAmount = bcoinRedeemAmount;
    }

    public Integer getCdpCloseCount() {
        return cdpCloseCount;
    }

    public void setCdpCloseCount(Integer cdpCloseCount) {
        this.cdpCloseCount = cdpCloseCount;
    }

    public Integer getCdpCreateCount() {
        return cdpCreateCount;
    }

    public void setCdpCreateCount(Integer cdpCreateCount) {
        this.cdpCreateCount = cdpCreateCount;
    }

    public Integer getCdpTotalActive() {
        return cdpTotalActive;
    }

    public void setCdpTotalActive(Integer cdpTotalActive) {
        this.cdpTotalActive = cdpTotalActive;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getScoinBurnAmount() {
        return scoinBurnAmount;
    }

    public void setScoinBurnAmount(java.math.BigDecimal scoinBurnAmount) {
        this.scoinBurnAmount = scoinBurnAmount;
    }

    public java.math.BigDecimal getScoinMintAmount() {
        return scoinMintAmount;
    }

    public void setScoinMintAmount(java.math.BigDecimal scoinMintAmount) {
        this.scoinMintAmount = scoinMintAmount;
    }

    public java.util.Date getStatsTime() {
        return statsTime;
    }

    public void setStatsTime(java.util.Date statsTime) {
        this.statsTime = statsTime;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public java.math.BigDecimal getWgrtBurnAmount() {
        return wgrtBurnAmount;
    }

    public void setWgrtBurnAmount(java.math.BigDecimal wgrtBurnAmount) {
        this.wgrtBurnAmount = wgrtBurnAmount;
    }

    public java.math.BigDecimal getWgrtIssueAmount() {
        return wgrtIssueAmount;
    }

    public void setWgrtIssueAmount(java.math.BigDecimal wgrtIssueAmount) {
        this.wgrtIssueAmount = wgrtIssueAmount;
    }

    public java.math.BigDecimal getWgrtPrice() {
        return wgrtPrice;
    }

    public void setWgrtPrice(java.math.BigDecimal wgrtPrice) {
        this.wgrtPrice = wgrtPrice;
    }

    public java.math.BigDecimal getWiccPrice() {
        return wiccPrice;
    }

    public void setWiccPrice(java.math.BigDecimal wiccPrice) {
        this.wiccPrice = wiccPrice;
    }

    public java.math.BigDecimal getWiccTotalLock() {
        return wiccTotalLock;
    }

    public void setWiccTotalLock(java.math.BigDecimal wiccTotalLock) {
        this.wiccTotalLock = wiccTotalLock;
    }

    public java.math.BigDecimal getWusdTotalSupply() {
        return wusdTotalSupply;
    }

    public void setWusdTotalSupply(java.math.BigDecimal wusdTotalSupply) {
        this.wusdTotalSupply = wusdTotalSupply;
    }

}

