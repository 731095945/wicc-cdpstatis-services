package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * DexTradeSettleTx is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class DexTradeSettleTx implements Serializable {

    @Column("asset_amount")
    private Long assetAmount;

    @Column("block_hash")
    private String blockHash;

    @Column("buy_order_id")
    private String buyOrderId;

    @Column("coin_amount")
    private Long coinAmount;

    @Column("confirmed_time")
    private Long confirmedTime;

    @Column("create_at")
    private java.util.Date createAt;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("txid")
    private String txid;

    @Column("tx_type")
    private String txType;

    @Column("update_at")
    private java.util.Date updateAt;

    public Long getAssetAmount() {
        return assetAmount;
    }

    public void setAssetAmount(Long assetAmount) {
        this.assetAmount = assetAmount;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public String getBuyOrderId() {
        return buyOrderId;
    }

    public void setBuyOrderId(String buyOrderId) {
        this.buyOrderId = buyOrderId;
    }

    public Long getCoinAmount() {
        return coinAmount;
    }

    public void setCoinAmount(Long coinAmount) {
        this.coinAmount = coinAmount;
    }

    public Long getConfirmedTime() {
        return confirmedTime;
    }

    public void setConfirmedTime(Long confirmedTime) {
        this.confirmedTime = confirmedTime;
    }

    public java.util.Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(java.util.Date createAt) {
        this.createAt = createAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public java.util.Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(java.util.Date updateAt) {
        this.updateAt = updateAt;
    }

}

