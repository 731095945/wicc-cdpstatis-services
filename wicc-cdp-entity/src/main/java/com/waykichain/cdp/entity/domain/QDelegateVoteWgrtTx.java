package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QDelegateVoteWgrtTx is a Querydsl query type for DelegateVoteWgrtTx
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QDelegateVoteWgrtTx extends com.querydsl.sql.RelationalPathBase<DelegateVoteWgrtTx> {

    private static final long serialVersionUID = 2111459376;

    public static final QDelegateVoteWgrtTx delegateVoteWgrtTx = new QDelegateVoteWgrtTx("delegate_vote_wgrt_tx");

    public final StringPath blockHash = createString("blockHash");

    public final StringPath candidateVotes = createString("candidateVotes");

    public final NumberPath<Long> confirmedHeight = createNumber("confirmedHeight", Long.class);

    public final NumberPath<Long> confirmedTime = createNumber("confirmedTime", Long.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath fromAddr = createString("fromAddr");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath receipts = createString("receipts");

    public final NumberPath<java.math.BigDecimal> totalVotes = createNumber("totalVotes", java.math.BigDecimal.class);

    public final StringPath txid = createString("txid");

    public final StringPath txType = createString("txType");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> wgrtReward = createNumber("wgrtReward", java.math.BigDecimal.class);

    public final com.querydsl.sql.PrimaryKey<DelegateVoteWgrtTx> primary = createPrimaryKey(id);

    public QDelegateVoteWgrtTx(String variable) {
        super(DelegateVoteWgrtTx.class, forVariable(variable), "null", "delegate_vote_wgrt_tx");
        addMetadata();
    }

    public QDelegateVoteWgrtTx(String variable, String schema, String table) {
        super(DelegateVoteWgrtTx.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QDelegateVoteWgrtTx(String variable, String schema) {
        super(DelegateVoteWgrtTx.class, forVariable(variable), schema, "delegate_vote_wgrt_tx");
        addMetadata();
    }

    public QDelegateVoteWgrtTx(Path<? extends DelegateVoteWgrtTx> path) {
        super(path.getType(), path.getMetadata(), "null", "delegate_vote_wgrt_tx");
        addMetadata();
    }

    public QDelegateVoteWgrtTx(PathMetadata metadata) {
        super(DelegateVoteWgrtTx.class, metadata, "null", "delegate_vote_wgrt_tx");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(blockHash, ColumnMetadata.named("block_hash").withIndex(5).ofType(Types.VARCHAR).withSize(128));
        addMetadata(candidateVotes, ColumnMetadata.named("candidate_votes").withIndex(4).ofType(Types.LONGVARCHAR).withSize(16777215));
        addMetadata(confirmedHeight, ColumnMetadata.named("confirmed_height").withIndex(7).ofType(Types.BIGINT).withSize(19));
        addMetadata(confirmedTime, ColumnMetadata.named("confirmed_time").withIndex(8).ofType(Types.BIGINT).withSize(19));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(12).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(fromAddr, ColumnMetadata.named("from_addr").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(receipts, ColumnMetadata.named("receipts").withIndex(6).ofType(Types.LONGVARCHAR).withSize(16777215));
        addMetadata(totalVotes, ColumnMetadata.named("total_votes").withIndex(11).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(txid, ColumnMetadata.named("txid").withIndex(9).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txType, ColumnMetadata.named("tx_type").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(13).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(wgrtReward, ColumnMetadata.named("wgrt_reward").withIndex(10).ofType(Types.DECIMAL).withSize(32).withDigits(8));
    }

}

