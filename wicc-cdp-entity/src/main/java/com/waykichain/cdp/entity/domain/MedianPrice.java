package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * MedianPrice is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class MedianPrice implements Serializable {

    @Column("block_height")
    private Integer blockHeight;

    @Column("coin_symbol")
    private String coinSymbol;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("currency_symbol")
    private String currencySymbol;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("median_price")
    private java.math.BigDecimal medianPrice;

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(Integer blockHeight) {
        this.blockHeight = blockHeight;
    }

    public String getCoinSymbol() {
        return coinSymbol;
    }

    public void setCoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getMedianPrice() {
        return medianPrice;
    }

    public void setMedianPrice(java.math.BigDecimal medianPrice) {
        this.medianPrice = medianPrice;
    }

}

