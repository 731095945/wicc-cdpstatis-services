package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * DelegateVoteWgrtTx is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class DelegateVoteWgrtTx implements Serializable {

    @Column("block_hash")
    private String blockHash;

    @Column("candidate_votes")
    private String candidateVotes;

    @Column("confirmed_height")
    private Long confirmedHeight;

    @Column("confirmed_time")
    private Long confirmedTime;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("from_addr")
    private String fromAddr;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("receipts")
    private String receipts;

    @Column("total_votes")
    private java.math.BigDecimal totalVotes;

    @Column("txid")
    private String txid;

    @Column("tx_type")
    private String txType;

    @Column("updated_at")
    private java.util.Date updatedAt;

    @Column("wgrt_reward")
    private java.math.BigDecimal wgrtReward;

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public String getCandidateVotes() {
        return candidateVotes;
    }

    public void setCandidateVotes(String candidateVotes) {
        this.candidateVotes = candidateVotes;
    }

    public Long getConfirmedHeight() {
        return confirmedHeight;
    }

    public void setConfirmedHeight(Long confirmedHeight) {
        this.confirmedHeight = confirmedHeight;
    }

    public Long getConfirmedTime() {
        return confirmedTime;
    }

    public void setConfirmedTime(Long confirmedTime) {
        this.confirmedTime = confirmedTime;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromAddr() {
        return fromAddr;
    }

    public void setFromAddr(String fromAddr) {
        this.fromAddr = fromAddr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceipts() {
        return receipts;
    }

    public void setReceipts(String receipts) {
        this.receipts = receipts;
    }

    public java.math.BigDecimal getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(java.math.BigDecimal totalVotes) {
        this.totalVotes = totalVotes;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public java.math.BigDecimal getWgrtReward() {
        return wgrtReward;
    }

    public void setWgrtReward(java.math.BigDecimal wgrtReward) {
        this.wgrtReward = wgrtReward;
    }

}

