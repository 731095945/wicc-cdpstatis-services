package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * PriceMedianWgrtTx is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class PriceMedianWgrtTx implements Serializable {

    @Column("bcoin_redeem_amount")
    private java.math.BigDecimal bcoinRedeemAmount;

    @Column("block_height")
    private Integer blockHeight;

    @Column("block_timestamp")
    private Long blockTimestamp;

    @Column("create_at")
    private java.util.Date createAt;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("receipt")
    private String receipt;

    @Column("scoin_burn_amount")
    private java.math.BigDecimal scoinBurnAmount;

    @Column("txid")
    private String txid;

    @Column("tx_type")
    private String txType;

    @Column("update_at")
    private java.util.Date updateAt;

    @Column("wgrt_issue_amount")
    private java.math.BigDecimal wgrtIssueAmount;

    public java.math.BigDecimal getBcoinRedeemAmount() {
        return bcoinRedeemAmount;
    }

    public void setBcoinRedeemAmount(java.math.BigDecimal bcoinRedeemAmount) {
        this.bcoinRedeemAmount = bcoinRedeemAmount;
    }

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(Integer blockHeight) {
        this.blockHeight = blockHeight;
    }

    public Long getBlockTimestamp() {
        return blockTimestamp;
    }

    public void setBlockTimestamp(Long blockTimestamp) {
        this.blockTimestamp = blockTimestamp;
    }

    public java.util.Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(java.util.Date createAt) {
        this.createAt = createAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public java.math.BigDecimal getScoinBurnAmount() {
        return scoinBurnAmount;
    }

    public void setScoinBurnAmount(java.math.BigDecimal scoinBurnAmount) {
        this.scoinBurnAmount = scoinBurnAmount;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public java.util.Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(java.util.Date updateAt) {
        this.updateAt = updateAt;
    }

    public java.math.BigDecimal getWgrtIssueAmount() {
        return wgrtIssueAmount;
    }

    public void setWgrtIssueAmount(java.math.BigDecimal wgrtIssueAmount) {
        this.wgrtIssueAmount = wgrtIssueAmount;
    }

}

