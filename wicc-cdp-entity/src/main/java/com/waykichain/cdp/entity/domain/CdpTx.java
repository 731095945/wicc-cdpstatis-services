package com.waykichain.cdp.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * CdpTx is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class CdpTx implements Serializable {

    @Column("bcoin_amount")
    private java.math.BigDecimal bcoinAmount;

    @Column("bcoin_redeem_amount")
    private java.math.BigDecimal bcoinRedeemAmount;

    @Column("bcoin_symbol")
    private String bcoinSymbol;

    @Column("block_height")
    private Integer blockHeight;

    @Column("block_timestamp")
    private Long blockTimestamp;

    @Column("cdp_created_at")
    private String cdpCreatedAt;

    @Column("cdp_id")
    private String cdpId;

    @Column("collateral_ratio")
    private java.math.BigDecimal collateralRatio;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("fees")
    private java.math.BigDecimal fees;

    @Column("fee_symbol")
    private String feeSymbol;

    @Column("from_status")
    private Integer fromStatus;

    @Column("from_wicc_amount")
    private java.math.BigDecimal fromWiccAmount;

    @Column("from_wusd_amount")
    private java.math.BigDecimal fromWusdAmount;

    @Column("id")
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column("owner_addr")
    private String ownerAddr;

    @Column("owner_uid")
    private String ownerUid;

    @Column("receipt")
    private String receipt;

    @Column("scoin_amount")
    private java.math.BigDecimal scoinAmount;

    @Column("scoin_burn_amount")
    private java.math.BigDecimal scoinBurnAmount;

    @Column("scoin_interest")
    private java.math.BigDecimal scoinInterest;

    @Column("scoin_penalty")
    private java.math.BigDecimal scoinPenalty;

    @Column("to_status")
    private Integer toStatus;

    @Column("to_wicc_amount")
    private java.math.BigDecimal toWiccAmount;

    @Column("to_wusd_amount")
    private java.math.BigDecimal toWusdAmount;

    @Column("txid")
    private String txid;

    @Column("tx_type")
    private String txType;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public java.math.BigDecimal getBcoinAmount() {
        return bcoinAmount;
    }

    public void setBcoinAmount(java.math.BigDecimal bcoinAmount) {
        this.bcoinAmount = bcoinAmount;
    }

    public java.math.BigDecimal getBcoinRedeemAmount() {
        return bcoinRedeemAmount;
    }

    public void setBcoinRedeemAmount(java.math.BigDecimal bcoinRedeemAmount) {
        this.bcoinRedeemAmount = bcoinRedeemAmount;
    }

    public String getBcoinSymbol() {
        return bcoinSymbol;
    }

    public void setBcoinSymbol(String bcoinSymbol) {
        this.bcoinSymbol = bcoinSymbol;
    }

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(Integer blockHeight) {
        this.blockHeight = blockHeight;
    }

    public Long getBlockTimestamp() {
        return blockTimestamp;
    }

    public void setBlockTimestamp(Long blockTimestamp) {
        this.blockTimestamp = blockTimestamp;
    }

    public String getCdpCreatedAt() {
        return cdpCreatedAt;
    }

    public void setCdpCreatedAt(String cdpCreatedAt) {
        this.cdpCreatedAt = cdpCreatedAt;
    }

    public String getCdpId() {
        return cdpId;
    }

    public void setCdpId(String cdpId) {
        this.cdpId = cdpId;
    }

    public java.math.BigDecimal getCollateralRatio() {
        return collateralRatio;
    }

    public void setCollateralRatio(java.math.BigDecimal collateralRatio) {
        this.collateralRatio = collateralRatio;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public java.math.BigDecimal getFees() {
        return fees;
    }

    public void setFees(java.math.BigDecimal fees) {
        this.fees = fees;
    }

    public String getFeeSymbol() {
        return feeSymbol;
    }

    public void setFeeSymbol(String feeSymbol) {
        this.feeSymbol = feeSymbol;
    }

    public Integer getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(Integer fromStatus) {
        this.fromStatus = fromStatus;
    }

    public java.math.BigDecimal getFromWiccAmount() {
        return fromWiccAmount;
    }

    public void setFromWiccAmount(java.math.BigDecimal fromWiccAmount) {
        this.fromWiccAmount = fromWiccAmount;
    }

    public java.math.BigDecimal getFromWusdAmount() {
        return fromWusdAmount;
    }

    public void setFromWusdAmount(java.math.BigDecimal fromWusdAmount) {
        this.fromWusdAmount = fromWusdAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwnerAddr() {
        return ownerAddr;
    }

    public void setOwnerAddr(String ownerAddr) {
        this.ownerAddr = ownerAddr;
    }

    public String getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(String ownerUid) {
        this.ownerUid = ownerUid;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public java.math.BigDecimal getScoinAmount() {
        return scoinAmount;
    }

    public void setScoinAmount(java.math.BigDecimal scoinAmount) {
        this.scoinAmount = scoinAmount;
    }

    public java.math.BigDecimal getScoinBurnAmount() {
        return scoinBurnAmount;
    }

    public void setScoinBurnAmount(java.math.BigDecimal scoinBurnAmount) {
        this.scoinBurnAmount = scoinBurnAmount;
    }

    public java.math.BigDecimal getScoinInterest() {
        return scoinInterest;
    }

    public void setScoinInterest(java.math.BigDecimal scoinInterest) {
        this.scoinInterest = scoinInterest;
    }

    public java.math.BigDecimal getScoinPenalty() {
        return scoinPenalty;
    }

    public void setScoinPenalty(java.math.BigDecimal scoinPenalty) {
        this.scoinPenalty = scoinPenalty;
    }

    public Integer getToStatus() {
        return toStatus;
    }

    public void setToStatus(Integer toStatus) {
        this.toStatus = toStatus;
    }

    public java.math.BigDecimal getToWiccAmount() {
        return toWiccAmount;
    }

    public void setToWiccAmount(java.math.BigDecimal toWiccAmount) {
        this.toWiccAmount = toWiccAmount;
    }

    public java.math.BigDecimal getToWusdAmount() {
        return toWusdAmount;
    }

    public void setToWusdAmount(java.math.BigDecimal toWusdAmount) {
        this.toWusdAmount = toWusdAmount;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

