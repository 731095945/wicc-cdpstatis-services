package com.waykichain.cdp.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPriceMedianWgrtTx is a Querydsl query type for PriceMedianWgrtTx
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QPriceMedianWgrtTx extends com.querydsl.sql.RelationalPathBase<PriceMedianWgrtTx> {

    private static final long serialVersionUID = 253483358;

    public static final QPriceMedianWgrtTx priceMedianWgrtTx = new QPriceMedianWgrtTx("price_median_wgrt_tx");

    public final NumberPath<java.math.BigDecimal> bcoinRedeemAmount = createNumber("bcoinRedeemAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> blockHeight = createNumber("blockHeight", Integer.class);

    public final NumberPath<Long> blockTimestamp = createNumber("blockTimestamp", Long.class);

    public final DateTimePath<java.util.Date> createAt = createDateTime("createAt", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath receipt = createString("receipt");

    public final NumberPath<java.math.BigDecimal> scoinBurnAmount = createNumber("scoinBurnAmount", java.math.BigDecimal.class);

    public final StringPath txid = createString("txid");

    public final StringPath txType = createString("txType");

    public final DateTimePath<java.util.Date> updateAt = createDateTime("updateAt", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> wgrtIssueAmount = createNumber("wgrtIssueAmount", java.math.BigDecimal.class);

    public final com.querydsl.sql.PrimaryKey<PriceMedianWgrtTx> primary = createPrimaryKey(id);

    public QPriceMedianWgrtTx(String variable) {
        super(PriceMedianWgrtTx.class, forVariable(variable), "null", "price_median_wgrt_tx");
        addMetadata();
    }

    public QPriceMedianWgrtTx(String variable, String schema, String table) {
        super(PriceMedianWgrtTx.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPriceMedianWgrtTx(String variable, String schema) {
        super(PriceMedianWgrtTx.class, forVariable(variable), schema, "price_median_wgrt_tx");
        addMetadata();
    }

    public QPriceMedianWgrtTx(Path<? extends PriceMedianWgrtTx> path) {
        super(path.getType(), path.getMetadata(), "null", "price_median_wgrt_tx");
        addMetadata();
    }

    public QPriceMedianWgrtTx(PathMetadata metadata) {
        super(PriceMedianWgrtTx.class, metadata, "null", "price_median_wgrt_tx");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(bcoinRedeemAmount, ColumnMetadata.named("bcoin_redeem_amount").withIndex(11).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(blockHeight, ColumnMetadata.named("block_height").withIndex(4).ofType(Types.INTEGER).withSize(10));
        addMetadata(blockTimestamp, ColumnMetadata.named("block_timestamp").withIndex(5).ofType(Types.BIGINT).withSize(19));
        addMetadata(createAt, ColumnMetadata.named("create_at").withIndex(9).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(19).notNull());
        addMetadata(receipt, ColumnMetadata.named("receipt").withIndex(6).ofType(Types.LONGVARCHAR).withSize(65535));
        addMetadata(scoinBurnAmount, ColumnMetadata.named("scoin_burn_amount").withIndex(8).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(txid, ColumnMetadata.named("txid").withIndex(2).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txType, ColumnMetadata.named("tx_type").withIndex(3).ofType(Types.VARCHAR).withSize(128));
        addMetadata(updateAt, ColumnMetadata.named("update_at").withIndex(10).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(wgrtIssueAmount, ColumnMetadata.named("wgrt_issue_amount").withIndex(7).ofType(Types.DECIMAL).withSize(32).withDigits(8));
    }

}

