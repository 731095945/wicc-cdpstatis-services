package com.waykichain.cdp.commons.biz.dict

/**
 * Created by mr_sun on 2017/11/27.
 */
enum class ErrorCode(val code: Int, val msg: String) {


    /** 系统错误*/
    SYS_INTERNAL_ERROR                (1000, "System error, please wait a moment"),

    /** 请求参数有误*/
    PARAM_ERROR                       (1001, "Request parameter error"),

    /** cdp相关*/
    CDP_INFO_NOT_EXIST                (3001, "The CDP information does not exist yet"),

    CDP_NODE_DOWN                      (3002, "The CDP node is down"),

    ;

    companion object {

        private val map = ErrorCode.values().associateBy  { errorCode: ErrorCode -> errorCode.code }

        fun getByCode(code: Int) = map[code]

        fun getMessage(code: Int): String {
            val result = map[code] ?: return ""
            return result.msg
        }
    }

}
