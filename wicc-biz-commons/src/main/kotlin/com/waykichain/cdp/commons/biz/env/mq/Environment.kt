package com.waykichain.cdp.commons.biz.env.mq

import com.waykichain.commons.util.BaseEnv
import org.slf4j.LoggerFactory

/**
 * Created by richardchen on 4/15/17.
 */
object Environment {
    val LOGGER = LoggerFactory.getLogger(Environment::class.java)!!

    /**
     * mysql com.waykichain.sports.entity.controller
     */
    @JvmField
    var MQ_HOST = BaseEnv.env("MQ_HOST", "10.0.0.4")
    @JvmField
    var MQ_PORT = BaseEnv.env("MQ_PORT", 5672)
    @JvmField
    var MQ_USERNAME = BaseEnv.env("MQ_USERNAME", "wicc-mq")
    @JvmField
    var MQ_PASSWORD = BaseEnv.env("MQ_PASSWORD", "wicc123456")
    @JvmField
    var MQ_VIRTUAL_HOST = BaseEnv.env("MQ_VIRTUAL_HOST", "wicc-bet")
    @JvmField
    var MQ_CONN_CACHE_SIZE = BaseEnv.env("MQ_CONN_CACHE_SIZE", 10)
    @JvmField
    var MQ_CONNECTION_SIZE = BaseEnv.env("MQ_CONNECTION_SIZE", 10)
    @JvmField
    var MQ_MAX_CONCURRENT_CONSUMER_SIZE = BaseEnv.env("MQ_MAX_CONCURRENT_CONSUMER_SIZE", 10)
    @JvmField
    var MQ_CONCURRENT_CONSUMER_SIZE = BaseEnv.env("MQ_CONCURRENT_CONSUMER_SIZE", 5)

    @JvmField
    var MQ_ROUTING_KEY_ALL_MESSAGE = BaseEnv.env("MQ_ROUTING_KEY_ALL_MESSAGE", "com.waykichain.bet.routing.key.*")
    @JvmField
    var MQ_EXCHANGE = BaseEnv.env("MQ_EXCHANGE", "com.waykichain.bet.exchange")

    ///<支付消息
    @JvmField
    var MQ_QUEUE_TRADING = BaseEnv.env("MQ_QUEUE_EINSTEIN", "com.waykichain.bet.queue.trading")
    @JvmField
    var MQ_ROUTING_KEY_ORDER_HEADER = BaseEnv.env("MQ_ROUTING_KEY_ORDER_HEADER", "com.waykichain.bet.routing.key.order")

}

