package com.waykichain.cdp.commons.biz.env.redis

import com.waykichain.commons.util.BaseEnv
import org.slf4j.LoggerFactory

/**
 * Created by richardchen on 4/15/17.
 */
object Environment {
    val LOGGER = LoggerFactory.getLogger(Environment::class.java)!!

    /**
     * mysql com.waykichain.sports.entity.controller
     */
    @JvmField
    var ACCESS_TOKEN_EXPIRE_SECONDS = BaseEnv.env("ACCESS_TOKEN_EXPIRE_SECONDS", 3600 * 24 * 3L)

    @JvmField
    var REFRESH_TOKEN_EXPIRE_SECONDS = BaseEnv.env("REFRESH_TOKEN_EXPIRE_SECONDS", 3600 * 24 * 7 * 3L)

    @JvmField val REDIS_DB_INDEX = BaseEnv.env("REDIS_DB_INDEX", 0)
    @JvmField val REDIS_HOST = BaseEnv.env("REDIS_HOST", "10.0.0.4")
    @JvmField val REDIS_PORT = BaseEnv.env("REDIS_PORT", 6379)
    @JvmField val REDIS_CONNECTION_TIMEOUT = BaseEnv.env("REDIS_CONNECTION_TIMEOUT", 10)

    @JvmField val REDIS_AUTH_PASSWORD = BaseEnv.env("REDIS_AUTH_PASSWORD", "wicc@123456")

    @JvmField
    val LOGIN_GEN_TOKEN_TIMEOUT = BaseEnv.env("LOGIN_GEN_TOKEN_TIMEOUT", 60 * 3L)// 单位秒



}

