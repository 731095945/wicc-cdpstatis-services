package com.waykichain.cdp.commons.biz.env.mysql

import com.waykichain.commons.util.BaseEnv
import org.slf4j.LoggerFactory

/**
 * Created by richardchen on 4/15/17.
 */
object Environment {
    val LOGGER = LoggerFactory.getLogger(Environment::class.java)!!

    /**
     * mysql com.waykichain.sports.entity.controller
     */

    @JvmField
    val MYSQL_URL = BaseEnv.env("MYSQL_URL", "jdbc:mysql://10.0.0.31:3306/wicc-cdp?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false")
    @JvmField
    val MYSQL_DRIVER = BaseEnv.env("MYSQL_DRIVER", "com.mysql.jdbc.Driver")
    @JvmField
    val MYSQL_USERNAME = BaseEnv.env("MYSQL_USERNAME", "wayki-rw")
    @JvmField
    val MYSQL_PASSWORD = BaseEnv.env("MYSQL_PASSWORD", "wayiC@r19w")

    @JvmField
    val MYSQL_INITIALSIZE = BaseEnv.env("MYSQL_INITIALSIZE", 10)
    @JvmField
    val MYSQL_MIN_IDLE = BaseEnv.env("MYSQL_MIN_IDLE", 5)
    @JvmField
    val MYSQL_MAX_ACTIVE = BaseEnv.env("MYSQL_MAX_ACTIVE", 20)
}

