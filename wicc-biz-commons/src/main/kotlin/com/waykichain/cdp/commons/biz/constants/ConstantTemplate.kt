package com.waykichain.cdp.commons.biz.constants

object ConstantTemplate{

    val NEWS_THIRDPART_KEY = "tk_%s_%d_%d"

    val BKB_NEWS_THIRDPART_KEY = "tk_%s_%s"


    val SENSITIVE_WORDS_REDIS: String = "sensitive_words"

    val SENSITIVE_WORDS_CHANGE_FLAG = "sensitive_words_change_flag"

    val NICKNAME_SENSITIVE_WORDS = arrayListOf<String>("维基链","维基社区","维基时代","击剑师","维基团队","维基官方","维基链wicc","wicc")
    val userRedisTag = "USER_FORBIDDEN_COMMENT_REDIS_TAG"
}