package com.waykichain.cdp.commons.biz.exception

import com.waykichain.cdp.commons.biz.dict.ErrorCode


/**
 * Created by richardchen on 4/22/17.
 */
class BizException(val code: Int, val msg: String) : RuntimeException(msg) {

    constructor(errorCode: ErrorCode) : this(errorCode.code, errorCode.msg)
}
