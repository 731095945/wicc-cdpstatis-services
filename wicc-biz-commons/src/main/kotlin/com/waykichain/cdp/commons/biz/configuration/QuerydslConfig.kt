package com.waykichain.cdp.commons.biz.configuration

import com.querydsl.jpa.impl.JPAQueryFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.persistence.EntityManager

/**
 * Created by richardchen on 28/03/2017.
 */
@Configuration
open class QuerydslConfig {

    @Autowired lateinit var entityManager: EntityManager

    @Bean
    open fun jpaQueryFactory(): JPAQueryFactory {
        return JPAQueryFactory(entityManager)
    }

}