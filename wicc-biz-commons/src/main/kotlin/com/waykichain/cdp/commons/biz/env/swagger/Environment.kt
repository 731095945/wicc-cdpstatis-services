package com.waykichain.cdp.commons.biz.env.swagger

import com.waykichain.commons.util.BaseEnv

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/4/24 17:39
 *
 * @Description:    swagger相关配置
 *
 */
object Environment {

    @JvmField
    var SWAGGER_WAPAPI_HOST = BaseEnv.env("SWAGGER_WAPAPI_HOST", "dev.wayki.cn/api")

    @JvmField
    var WICC_SERVICE_ENABLE_SWAGGER = BaseEnv.env("WICC_SERVICE_ENABLE_SWAGGER", true)

    @JvmField
    var SWAGGER_WABAPI_TITLE = BaseEnv.env("SWAGGER_WABAPI_TITLE", "Waykichain Platform API")


    @JvmField
    var SWAGGER_WABAPI_DESCRIPTION = BaseEnv.env("SWAGGER_WABAPI_DESCRIPTION", "Waykichain Platform's REST API, all the applications could assess the Object model data via JSON.")

    @JvmField
    var SWAGGER_WABAPI_VERSION = BaseEnv.env("SWAGGER_WABAPI_VERSION", "1.0")



    @JvmField
    var SWAGGER_WABAPI_SERVICEURL = BaseEnv.env("SWAGGER_WABAPI_SERVICEURL", "Waykichain Platform API")


    @JvmField
    var SWAGGER_WABAPI_LICENSE = BaseEnv.env("SWAGGER_WABAPI_LICENSE", "Commercial License, Version 1.0")

    @JvmField
    var SWAGGER_WABAPI_LICENSE_URL = BaseEnv.env("SWAGGER_WABAPI_LICENSE_URL", "http://www.apache.org/licenses/LICENSE-2.0.html")
}