package com.waykichain.cdp.commons.biz.env

import com.waykichain.commons.util.BaseEnv
import org.slf4j.LoggerFactory

/**
 * Created by richardchen on 4/15/17.
 */
object Environment {
    val LOGGER = LoggerFactory.getLogger(Environment::class.java)!!

    @JvmField
    var RED_PACKET_POOL_ADDRESS = BaseEnv.env("RED_PACKET_POOL_ADDRESS", "wWEm2DY4cfFdqGpe1J5ZRdizZHN7ct1rLq")

    @JvmField
    var COIN_WALLET_SERVICE_URL = BaseEnv.env("COIN_WALLET_SERVICE_URL", "http://10.0.0.6:21052")

    @JvmField
    var WTIMES_ICO_CONTRACT_REGID = BaseEnv.env("WTIMES_ICO_CONTRACT_REGID", "1304166-1")

    @JvmField
    var WTIMES_ICO_CONTRACT_ADDRESS = BaseEnv.env("WTIMES_ICO_CONTRACT_ADDRESS", "WmubVEJ7Kzn38DfLmC4b8ms7gS58Y1QQFU")

    @JvmField
    var RED_PACKET_SHARE_DOMAIN = BaseEnv.env("RED_PACKET_SHARE_DOMAIN", "hongbao.fadacai.art")

    @JvmField var WICC_SERVICE_ENABLE_SWAGGER = BaseEnv.env("WICC_SERVICE_ENABLE_SWAGGER", true)

    @JvmField var WICC_SERVICE_ENVIRONMENT = BaseEnv.env("WICC_SERVICE_ENVIRONMENT", "dev")

    @JvmField var COINDOG_ACCESS_KEY = BaseEnv.env("COINDOG_ACCESS_KEY", "924246b11596bb6c213983cf4e048b8c")

    @JvmField var COINDOG_SECRET_KEY = BaseEnv.env("COINDOG_ACCESS_KEY","2e457ea5266b305c")

    @JvmField var COINDOG_TOPIC_URL = BaseEnv.env("COINDOG_TOPIC_URL", "http://api.coindog.com/topic/list")

    @JvmField var COINDOG_LIVE_URL = BaseEnv.env( "COINDOG_LIVE_URL", "http://api.coindog.com/live/list")

    @JvmField var BEEKUAIBAO_KEY = BaseEnv.env("BEEKUAIBAO_KEY","07360FD5946F9F7620942E18EA9EFF48")

    @JvmField var BEEKUAIBAO_URL = BaseEnv.env("BEEKUAIBAO_URL", "https://api.beekuaibao.com/thirdparty/getOpenData")

    @JvmField var BISHIJIE_URL = BaseEnv.env("BISHIJIE_URL", "https://iapi.bishijie.com")

    @JvmField var BISHIJIE_APPID = BaseEnv.env("BISHIJIE_APPID", "6175bd814fa1f5da")

    @JvmField var BISHIJIE_SECRETKEY = BaseEnv.env("BISHIJIE_SECRETKEY", "4bf2fd18e89382883befe2164a9adac4")

    @JvmField var REWARD_URL = BaseEnv.env("REWARD_URL", "http://47.106.164.177:8012/times/sendtimesprizes")

    @JvmField var REWARD_PASSWD = BaseEnv.env("REWARD_PASSWD", "yexiao2018")

    /**
     * wicc实时美元价格
     * https://www.aex88.com/bitBaike/coinDetailFetch.php?coinname=WIC
     */
    @JvmField
    var AEX_WICC_PRICE_AEX_API_URL = BaseEnv.env("AEX_WICC_PRICE_AEX_API_URL", "https://api.coinmarketcap.com/v1/ticker/waykichain/?convert=CNY")

    /**
     * 美元汇率第三方接口
     * 免费试用三个月，有效期至2018-12-15
     */
    @JvmField
    var K780_EXCHAGE_USD_RATE_URL = BaseEnv.env("K780_EXCHAGE_USD_RATE_URL", "http://api.k780.com/?app=finance.rate&scur=USD&tcur=CNY&appkey=36423&sign=5db06683babcbdd6b7d12ab5983e1203")

    /**
     * 根据ip查询归属地
     * example: http://ip-api.com/json/14.0.16.1
     */
    @JvmField
    var IP_API_URL = BaseEnv.env("IP_API_URL","http://ip-api.com/json/")

    /**
     * 根据ip查询归属地
     * example: http://ip-api.com/json/14.0.16.1
     */
    @JvmField
    var HUOBI_API_URL = BaseEnv.env("HUOBI_API_URL","http://comm.waykitest.com/huobiapi/market/trade?symbol=wiccusdt")

    @JvmField
    var LIQUATE_RATE = BaseEnv.env("LIQUATE_RATE", 150)

    @JvmField
    var PENALTY = BaseEnv.env("PENALTY", 13f)

    @JvmField
    var WUSDFEE = BaseEnv.env("WUSDFEE", 0.002f)


}

