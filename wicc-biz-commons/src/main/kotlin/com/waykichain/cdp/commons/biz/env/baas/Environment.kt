package com.waykichain.cdp.commons.biz.env.baas

import com.waykichain.commons.util.BaseEnv
import org.slf4j.LoggerFactory

object Environment{

        @JvmField
        var COIN_WALLET_SERVICE_URL = BaseEnv.env("COIN_WALLET_SERVICE_URL", "http://10.0.0.6:21052")
//        var COIN_WALLET_SERVICE_URL = BaseEnv.env("COIN_WALLET_SERVICE_URL", "https://baas.wiccdev.org/v1/api")

}