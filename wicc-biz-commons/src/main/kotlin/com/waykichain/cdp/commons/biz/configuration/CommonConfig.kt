package com.waykichain.cdp.commons.biz.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order

/**
 * Created by Joss on 2018/08/17.
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
open class CommonConfig {

    @Bean(initMethod = "init")
    open fun sysConfigLoadManager(): SysConfigLoadManager {
        return SysConfigLoadManager()
    }

}