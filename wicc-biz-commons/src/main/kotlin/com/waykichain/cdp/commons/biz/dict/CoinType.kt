package com.waykichain.cdp.commons.biz.dict

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/5/5 13:42
 *
 * @Description:    $des
 *
 */
enum class CoinType (val code:Int, val msg: String) {
    WICC(100, "WICC"),
    WGRT(200, "WGRT"),
    WUSD(400, "WUSD"),
    RMB(500, "RMB"),
    USD(501, "USD"),

    WLC(601, "WLC"),
    WICC_FROZEN(603, "WICC_FROZEN"),

    BTC(605, "BTC"),
    ETH(607, "ETH"),
    USDT(608, "USDT")


}