package com.waykichain.cdp.commons.biz.env.task

import com.waykichain.commons.util.BaseEnv
import org.slf4j.LoggerFactory

/**
 * Created by richardchen on 4/15/17.
 */
object Environment {
    val LOGGER = LoggerFactory.getLogger(Environment::class.java)!!

    /**
     * mysql com.waykichain.sports.entity.controller
     */
    /**
     * 定时任务
     */
    @JvmField
    var TASK_JOB_ADMIN_URL= BaseEnv.env("TASK_JOB_ADMIN_URL", "http://10.0.0.31:8888")
    @JvmField
    var TASK_APP_NAME = BaseEnv.env("TASK_APP_NAME", "cdp-task-executor")
    @JvmField
    var TASK_EXECUTOR_IP= BaseEnv.env("TASK_EXECUTOR_IP", "172.18.195.230")
    @JvmField
    var TASK_EXECUTOR_PORT = BaseEnv.env("TASK_EXECUTOR_PORT", "9999") //RPC Port
    @JvmField
    var TASK_LOG_PATH= BaseEnv.env("TASK_LOG_PATH", "/tmp/wicc/logs")

    @JvmField
    val MARKET_API_URL = BaseEnv.env("MARKET_API_URL", "https://api.coinmarketcap.com/v2/ticker/2346/")


}

