package com.waykichain.cdp.xservice

import com.waykichain.cdp.commons.biz.domain.po.CdpStatPO
import com.waykichain.cdp.commons.biz.domain.vo.*
import com.waykichain.cdp.entity.domain.CdpStats

interface StatisticsXService {

    fun getWusd(po: CdpStatPO): WusdListVO

    fun getWicc(po: CdpStatPO): WiccListVO

    fun getWgrt(po: CdpStatPO): WgrtListVO

    fun getCdp(po: CdpStatPO): CdpStatisticsListVO

    fun updateWusd(po: CdpStatPO): WusdListVO

    fun updateWicc(po: CdpStatPO): WiccListVO

    fun updateWgrt(po: CdpStatPO): WgrtListVO

    fun updateCdp(po: CdpStatPO): CdpStatisticsListVO

    fun updateWiccInfo():WiccInfoVO

    fun updateWusdInfo(): WusdInfoVO

    fun updateCdpInfo():CdpInfoVO

    fun updateWgrtInfo():WgrtInfoVO

    fun getWusdInfo(): WusdInfoVO

    fun getWiccInfo(): WiccInfoVO

    fun getCdpInfo(): CdpInfoVO

    fun getWgrtInfo(): WgrtInfoVO

    fun getDataInfo(po: CdpStatPO): DataInfoVO

    fun updateCdpStatisDB()

    fun refreshData(day:String): CdpStats


}