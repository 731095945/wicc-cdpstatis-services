package com.waykichain.cdp.xservice.impl

import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.waykichain.biz.redis.repository.impl.ValueCacheRedisRepository
import com.waykichain.cdp.commons.biz.dict.CdpStatisType
import com.waykichain.cdp.commons.biz.dict.CoinType
import com.waykichain.cdp.commons.biz.dict.TimeIntervalType
import com.waykichain.cdp.commons.biz.domain.beans.CdpStatsInfo
import com.waykichain.cdp.commons.biz.domain.constants.CdpRedisKeys
import com.waykichain.cdp.commons.biz.domain.po.CdpStatPO
import com.waykichain.cdp.commons.biz.domain.vo.*
import com.waykichain.cdp.commons.biz.util.AmountOperateUtil
import com.waykichain.cdp.commons.biz.util.DateTimeUtil
import com.waykichain.cdp.entity.domain.CdpStats
import com.waykichain.cdp.entity.domain.QCdpStats
import com.waykichain.cdp.repository.CdpStatsRepository
import com.waykichain.cdp.repository.DelegateVoteWgrtTxRepository
import com.waykichain.cdp.xservice.StatisticsXService
import com.waykichain.commons.util.DateUtils
import com.waykichain.commons.util.WiccUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext


@Service
class StatisticsXServiceImpl: StatisticsXService {

    

    override fun getWusdInfo(): WusdInfoVO {
        val str = valueCacheRedisRepository.get(CdpRedisKeys.CDP_STATS_WUSD_INFO_REDISKEY)
        if(str != null) return Gson().fromJson(str, WusdInfoVO::class.java)
        else return updateWusdInfo()
    }

    override fun updateWusdInfo(): WusdInfoVO {
        val wusdInfoVO = WusdInfoVO()
        wusdInfoVO.currentSupply = cdpStatisticsRepository.getCurrentSupply()
        wusdInfoVO.wusdGenerated = cdpStatisticsRepository.getGeneratedWUSD().stripTrailingZeros().toPlainString()
        wusdInfoVO.wusdBurned = cdpStatisticsRepository.getBurnedWUSD().plus(cdpStatisticsRepository.getBurnWusdFromForceLiquate()).stripTrailingZeros().toPlainString()
        valueCacheRedisRepository.put(CdpRedisKeys.CDP_STATS_WUSD_INFO_REDISKEY, JSONObject.toJSONString(wusdInfoVO))
        return wusdInfoVO
    }

    override fun getWiccInfo(): WiccInfoVO {
        val str = valueCacheRedisRepository.get(CdpRedisKeys.CDP_STATS_WICC_INFO_REDISKEY)
        if(str != null) return Gson().fromJson(str, WiccInfoVO::class.java)
        else return updateWiccInfo()
    }

    override fun getWgrtInfo(): WgrtInfoVO {
        val str = valueCacheRedisRepository.get(CdpRedisKeys.CDP_STATS_WGRT_INFO_REDISKEY)
        if(str != null) return Gson().fromJson(str, WgrtInfoVO::class.java)
        else return updateWgrtInfo()
    }

    override fun getDataInfo(po: CdpStatPO): DataInfoVO {
        val dataInfo = DataInfoVO()
        dataInfo.wiccInfoVO = getWiccInfo()
        dataInfo.wusdInfoVO = getWusdInfo()
        dataInfo.cdpInfoVO = getCdpInfo()
        dataInfo.wgrtInfo = getWgrtInfo()
        when(po.type) {
            CdpStatisType.WICC.description -> {
                for (wiccVo in getWicc(po).list){
                    val statisVO = StatisVO()
                    statisVO.add = wiccVo.wiccAddtion.stripTrailingZeros().toPlainString()
                    statisVO.minus = wiccVo.wiccRedemption.negate().stripTrailingZeros().toPlainString()
                    statisVO.time = wiccVo.time
                    statisVO.sum = wiccVo.wiccTotalLock
                    statisVO.price = wiccVo.price
                    dataInfo.statisticsList.add(statisVO)
                }
            }
            CdpStatisType.WUSD.description -> {
                for (wusdVo in getWusd(po).list){
                    val statisVO = StatisVO()
                    statisVO.add = wusdVo.wusdGenerated.stripTrailingZeros().toPlainString()
                    statisVO.minus = wusdVo.wusdBurned.negate().stripTrailingZeros().toPlainString()
                    statisVO.time = wusdVo.time
                    statisVO.price = wusdVo.price
                    statisVO.sum = wusdVo.wusdTotalSupply
                    dataInfo.statisticsList.add(statisVO)
                }
            }
            CdpStatisType.CDP.description -> {
                for(cdpStatisticsVO in  getCdp(po).list) {
                    val statisVO = StatisVO()
                    statisVO.add = cdpStatisticsVO.cdpAddition.toString()
                    statisVO.minus = BigDecimal(cdpStatisticsVO.cdpClose).negate().stripTrailingZeros().toPlainString()
                    statisVO.time = cdpStatisticsVO.time
                    statisVO.price = cdpStatisticsVO.price
                    statisVO.sum = cdpStatisticsVO.cdpTotalActive.toBigDecimal()
                    dataInfo.statisticsList.add(statisVO)
                }
            }
            CdpStatisType.WGRT.description -> {
                for(wgrtVO in  getWgrt(po).list) {
                    val statisVO = StatisVO()
                    statisVO.add = wgrtVO.wgrtIssueAmount.stripTrailingZeros().toPlainString()
                    statisVO.minus = wgrtVO.wgrtBurnAmount.negate().stripTrailingZeros().toPlainString()
                    statisVO.time = wgrtVO.time
                    statisVO.price = wgrtVO.price
                    dataInfo.statisticsList.add(statisVO)
                }
            }
        }

        return dataInfo
    }

    override fun updateWiccInfo(): WiccInfoVO {
        val wiccInfoVO = WiccInfoVO()
        wiccInfoVO.wiccLocked = cdpStatisticsRepository.getLockWICC()
        wiccInfoVO.wiccAddition = cdpStatisticsRepository.getAddWICC().stripTrailingZeros().toPlainString()
        wiccInfoVO.redemption = cdpStatisticsRepository.getRedemptWICC().plus(cdpStatisticsRepository.getWiccRedemptionBefore24H()).stripTrailingZeros().toPlainString()
        valueCacheRedisRepository.put(CdpRedisKeys.CDP_STATS_WICC_INFO_REDISKEY, JSONObject.toJSONString(wiccInfoVO))
        return  wiccInfoVO
    }

    override fun updateWgrtInfo(): WgrtInfoVO {
        val wgrtInfoVO = WgrtInfoVO()
//        wgrtInfoVO.wgrtTotalIssue = CdpConstant.CDP_WGRT_TOTAL_ISSUE.plus(cdpStatisticsRepository.getWgrtTotal())
//        wgrtInfoVO.wgrtTotalBurned = WiccUtils.convert(cdpStatisticsRepository.getTotalWgrtBurn().toLong())
//        wgrtInfoVO.wgrtBurnAmount = WiccUtils.convert(cdpStatisticsRepository.getWgrtBurn().toLong())
//        val  wgrtPrice = cdpStatisticsRepository.getWgrtPrice()
//        val  wgrtPriceBefore   = cdpStatisticsRepository.getWgrtPriceBefore24H()
//        if(wgrtPriceBefore.compareTo(BigDecimal.ZERO)!=0)wgrtInfoVO.wgrtChageRate = wgrtPrice.minus(wgrtPriceBefore).divide(wgrtPriceBefore,8, BigDecimal.ROUND_DOWN)
//        valueCacheRedisRepository.put(CdpRedisKeys.CDP_STATS_WGRT_INFO_REDISKEY, JSONObject.toJSONString(wgrtInfoVO))
        return  wgrtInfoVO
    }

    override fun getCdpInfo(): CdpInfoVO {
        val str = valueCacheRedisRepository.get(CdpRedisKeys.CDP_STATS_CDP_INFO_REDISKEY)
        if(str != null) return Gson().fromJson(str, CdpInfoVO::class.java)
        else return updateCdpInfo()
    }

    override fun updateCdpInfo(): CdpInfoVO {
        val cdpInfoVO = CdpInfoVO()
        Gson().fromJson(valueCacheRedisRepository.get(CdpRedisKeys.CDP_STATS_INFO_REDISKEY), CdpStatsInfo::class.java)?.let {
            cdpInfoVO.globalCR = it.totalCollateralRatio.replace("%", "").toBigDecimal().stripTrailingZeros().toPlainString()
        }
        cdpInfoVO.globalCdp = cdpStatisticsRepository.getActiveCDP()
        cdpInfoVO.addCdp = cdpStatisticsRepository.getAddCDP()
        cdpInfoVO.closeCdp = cdpStatisticsRepository.getCloseCDP()
        valueCacheRedisRepository.put(CdpRedisKeys.CDP_STATS_CDP_INFO_REDISKEY, JSONObject.toJSONString(cdpInfoVO))
        return cdpInfoVO
    }

    override fun getWusd(po: CdpStatPO): WusdListVO {
        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_WUSD_REDISKEY, po.timeIntervalType))
        if(str != null)
            return  Gson().fromJson(str, WusdListVO::class.java)
        else {
            return updateWusd(po)
        }
    }

    override fun getWicc(po: CdpStatPO): WiccListVO {
        val wiccList: WiccListVO
        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_WICC_REDISKEY, po.timeIntervalType))
        if(str != null)
            wiccList =  Gson().fromJson(str, WiccListVO::class.java)
        else {
            wiccList = updateWicc(po)
        }
        return wiccList
    }

    override fun getWgrt(po: CdpStatPO): WgrtListVO {
        val wgrtList: WgrtListVO
        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_WGRT_REDISKEY, po.timeIntervalType))
        if(str != null)
            wgrtList =  Gson().fromJson(str, WgrtListVO::class.java)
        else {
            wgrtList = updateWgrt(po)
        }
        return wgrtList
    }




    override fun getCdp(po: CdpStatPO): CdpStatisticsListVO {
        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_TOTAL_REDISKEY, po.timeIntervalType))
        if(str != null)
            return Gson().fromJson(str, CdpStatisticsListVO::class.java)
        else {
            return updateCdp(po)
        }
    }

    override fun refreshData(day:String):CdpStats {
        val wiccCdpStatistics = CdpStats()
        try {
            //cdpStatisticsRepository.flush()
            //var session = entityManager!!.unwrap(org.hibernate.Session::class.java)
//            session.
//            session.
            //cdpStatisticsRepository.setTimeZone()
            val wusdGenerated = cdpStatisticsRepository.getWusdGenerated(day)
            val wusdBurnned = cdpStatisticsRepository.getWusdBurned(day) + cdpStatisticsRepository.getWusdBurnFromForceLiquate(day)

            val wusdTotalGenerated = cdpStatisticsRepository.getGenerateWusdByDay(day)
            val wusdTotalBurnned = cdpStatisticsRepository.getBurnedWusdByDay(day) + cdpStatisticsRepository.getBurnWusdFromForceLiquateByDay(day)

            val wiccAddtion = cdpStatisticsRepository.getWiccAddtion(day)
            val wiccRedemption = cdpStatisticsRepository.getWiccRedemption(day) + cdpStatisticsRepository.getWiccRedemptionFromForLiquate(day)

            val wiccTotalAddtion = cdpStatisticsRepository.getWiccAddtionByDay(day)
            val wiccTotalRedemption = cdpStatisticsRepository.getWiccRedemptionByDay(day) + cdpStatisticsRepository.getWiccRedemptionFromForLiquateByDay(day)

            val cdpAdd = cdpStatisticsRepository.getCdpAdded(day)
            val cdpClose = cdpStatisticsRepository.getCdpClosed(day)

            val cdpTotalAdd = cdpStatisticsRepository.getCdpAddedByDay(day)
            val cdpTotalClose = cdpStatisticsRepository.getCdpClosedByDay(day)

            val wgrtIssueAmount = cdpStatisticsRepository.getWgrtIssueAmount(day).plus(delegateVoteWgrtTxRepository.getWgrtIssueAmount(day))
            val wgrtBurnAmount = WiccUtils.convert(cdpStatisticsRepository.getWgrtBurnAmount(day).toLong())

            val sumWiccPrice = cdpStatisticsRepository.getSumPrice(day, CoinType.WICC.msg)
            val wiccCount = cdpStatisticsRepository.getCountPrice(day, CoinType.WICC.msg)
            val sumWgrtPrice = cdpStatisticsRepository.getSumPrice(day, CoinType.WGRT.msg)
            val wgrtCount = cdpStatisticsRepository.getCountPrice(day, CoinType.WGRT.msg)

            var avgWiccPrice = BigDecimal.ZERO
            var avgWgrtPrice = BigDecimal.ZERO
            if (wiccCount.toInt() != 0) avgWiccPrice = sumWiccPrice.divide(BigDecimal(wiccCount.toInt()), 8, BigDecimal.ROUND_HALF_UP)
            if (wgrtCount.toInt() != 0) avgWgrtPrice = sumWgrtPrice.divide(BigDecimal(wgrtCount.toInt()), 8, BigDecimal.ROUND_HALF_UP)
            val predicate = QCdpStats.cdpStats.statsTime.eq(DateTimeUtil.getDate(day))

            val data = cdpStatisticsRepository.findOne(predicate)

            if (data !== null) wiccCdpStatistics.id = data.id

            wiccCdpStatistics.wusdTotalSupply = wusdTotalGenerated - wusdTotalBurnned
            wiccCdpStatistics.wiccTotalLock = wiccTotalAddtion - wiccTotalRedemption
            wiccCdpStatistics.cdpTotalActive = cdpTotalAdd - cdpTotalClose
            wiccCdpStatistics.scoinMintAmount = wusdGenerated
            wiccCdpStatistics.scoinBurnAmount = wusdBurnned
            wiccCdpStatistics.bcoinCollateralAmount = wiccAddtion
            wiccCdpStatistics.bcoinRedeemAmount = wiccRedemption
            wiccCdpStatistics.cdpCreateCount = cdpAdd
            wiccCdpStatistics.cdpCloseCount = cdpClose
            wiccCdpStatistics.statsTime = DateTimeUtil.getDate(day)
            wiccCdpStatistics.wiccPrice = AmountOperateUtil.getPriceFromBigDec(avgWiccPrice)
            wiccCdpStatistics.wgrtPrice = AmountOperateUtil.getPriceFromBigDec(avgWgrtPrice)
            wiccCdpStatistics.wgrtIssueAmount = wgrtIssueAmount
            wiccCdpStatistics.wgrtBurnAmount = wgrtBurnAmount
            cdpStatisticsRepository.saveAndFlush(wiccCdpStatistics)
           // cdpStatisticsRepository.clearTimeZone()
        } catch (e: Exception) {
            logger.error("StatisticsXServiceImpl error:", e)
        }
        return wiccCdpStatistics
    }

    override fun updateCdpStatisDB() {
         var start = cdpStatisticsRepository.getMaxCdpStatsDay()
         if(start == null) start = cdpStatisticsRepository.getMinCdpTxDay()
         if(start == null) start = Date()
         val end = Date()
         val list = DateTimeUtil.dateSplit(start, end).sortedBy { it }
         for(it in list) {
             val date = DateTimeUtil.getDayOfStr(it)
             refreshData(date)
         }
    }

    override fun updateWusd(po: CdpStatPO): WusdListVO {
        var wusdList = WusdListVO()
        val cdpStatis =  refreshData(DateTimeUtil.getToday())
        DateTimeUtil.setStartEndTime(po)
        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_WUSD_REDISKEY, po.timeIntervalType))
        if(str != null) {
            wusdList = Gson().fromJson(str, WusdListVO::class.java)

            if((po.timeIntervalType == TimeIntervalType.ONE_WEEK.description && wusdList.list.size >= 7 || po.timeIntervalType == TimeIntervalType.ONE_MONTH.description && wusdList.list.size >=30)
                    && wusdList.list[wusdList.list.size-1].time != DateTimeUtil.getToday())
                wusdList.list.removeAt(0)

            if(wusdList.list[wusdList.list.size-1].time == DateTimeUtil.getToday())
                wusdList.list.removeAt(wusdList.list.size - 1)
            val detail =  WusdVO()
            detail.wusdBurned = cdpStatis.scoinBurnAmount
            detail.wusdGenerated = cdpStatis.scoinMintAmount
            detail.time = DateUtils.dateFormat(cdpStatis.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
            detail.price = cdpStatis.wiccPrice
            detail.wusdTotalSupply = cdpStatis.wusdTotalSupply
            wusdList.list.add(detail)
        } else {
            val list = cdpStatisticsRepository.getCdpStats(po.startTime, po.endTime)
            list.let {
                for (bean in it) {
                    val detail = WusdVO()
                    detail.wusdBurned = bean.scoinBurnAmount
                    detail.wusdGenerated = bean.scoinMintAmount
                    detail.time = DateUtils.dateFormat(bean.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
                    detail.price = bean.wiccPrice
                    detail.wusdTotalSupply = bean.wusdTotalSupply
                    wusdList.list.add(detail)
                }
            }
        }

        valueCacheRedisRepository.put(String.format(CdpRedisKeys.CDP_STATS_WUSD_REDISKEY, po.timeIntervalType), JSONObject.toJSONString(wusdList))

        return wusdList
    }

    override fun updateWicc(po: CdpStatPO): WiccListVO {
        var wiccList = WiccListVO()
        val cdpStatis = refreshData(DateTimeUtil.getToday())
        DateTimeUtil.setStartEndTime(po)

        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_WICC_REDISKEY, po.timeIntervalType))
        if(str != null) {
            wiccList = Gson().fromJson(str, WiccListVO::class.java)
            if((po.timeIntervalType == TimeIntervalType.ONE_WEEK.description && wiccList.list.size >= 7 || po.timeIntervalType == TimeIntervalType.ONE_MONTH.description && wiccList.list.size >=30)
                    && wiccList.list[wiccList.list.size-1].time != DateTimeUtil.getToday())
                wiccList.list.removeAt(0)
            if(wiccList.list[wiccList.list.size-1].time == DateTimeUtil.getToday()) wiccList.list.removeAt(wiccList.list.size - 1)

            val detail = WiccVO()
            detail.time = DateUtils.dateFormat(cdpStatis.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
            detail.wiccAddtion = cdpStatis.bcoinCollateralAmount
            detail.wiccRedemption = cdpStatis.bcoinRedeemAmount
            detail.price = cdpStatis.wiccPrice
            detail.wiccTotalLock = cdpStatis.wiccTotalLock
            wiccList.list.add(detail)
        } else {
            val list = cdpStatisticsRepository.getCdpStats(po.startTime, po.endTime)
            list.let {
                for (bean in it) {
                    val detail = WiccVO()
                    BeanUtils.copyProperties(bean, detail)
                    detail.time = DateUtils.dateFormat(bean.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
                    detail.wiccAddtion = bean.bcoinCollateralAmount
                    detail.wiccRedemption = bean.bcoinRedeemAmount
                    detail.price = bean.wiccPrice
                    detail.wiccTotalLock = bean.wiccTotalLock
                    wiccList.list.add(detail)
                }
            }
        }
        valueCacheRedisRepository.put(String.format(CdpRedisKeys.CDP_STATS_WICC_REDISKEY, po.timeIntervalType), JSONObject.toJSONString(wiccList))
        return  wiccList
    }

    override fun updateWgrt(po: CdpStatPO): WgrtListVO {
        var wgrtList = WgrtListVO()
        val cdpStatis = refreshData(DateTimeUtil.getToday())
        DateTimeUtil.setStartEndTime(po)
        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_WGRT_REDISKEY, po.timeIntervalType))
        if(str != null) {
            wgrtList = Gson().fromJson(str, WgrtListVO::class.java)

            if((po.timeIntervalType == TimeIntervalType.ONE_WEEK.description && wgrtList.list.size >= 7 || po.timeIntervalType == TimeIntervalType.ONE_MONTH.description && wgrtList.list.size >=30)
                    && wgrtList.list[wgrtList.list.size-1].time != DateTimeUtil.getToday())
                wgrtList.list.removeAt(0)

            if(wgrtList.list[wgrtList.list.size-1].time == DateTimeUtil.getToday()) wgrtList.list.removeAt(wgrtList.list.size - 1)
            val detail = WgrtVO()
            detail.time = DateUtils.dateFormat(cdpStatis.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
            detail.wgrtIssueAmount = cdpStatis.wgrtIssueAmount
            detail.wgrtBurnAmount = cdpStatis.wgrtBurnAmount
            detail.price = cdpStatis.wiccPrice
            wgrtList.list.add(detail)
        } else {
            val list = this.cdpStatisticsRepository.getCdpStats(po.startTime, po.endTime)
            list.let {
                for (bean in it) {
                    val detail = WgrtVO()
                    BeanUtils.copyProperties(bean, detail)
                    detail.time = DateUtils.dateFormat(bean.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
                    detail.wgrtIssueAmount = bean.wgrtIssueAmount
                    detail.wgrtBurnAmount = bean.wgrtBurnAmount
                    detail.price = bean.wiccPrice
                    wgrtList.list.add(detail)
                }
            }
        }
        valueCacheRedisRepository.put(String.format(CdpRedisKeys.CDP_STATS_WGRT_REDISKEY, po.timeIntervalType), JSONObject.toJSONString(wgrtList))
        return  wgrtList
    }


    override fun updateCdp(po: CdpStatPO): CdpStatisticsListVO {
        var cdpStatisticsList = CdpStatisticsListVO()
        val cdpStatis = refreshData(DateTimeUtil.getToday())
        DateTimeUtil.setStartEndTime(po)

        val str = valueCacheRedisRepository.get(String.format(CdpRedisKeys.CDP_STATS_TOTAL_REDISKEY, po.timeIntervalType))
        if(str != null) {
            cdpStatisticsList = Gson().fromJson(str, CdpStatisticsListVO::class.java)

            if((po.timeIntervalType == TimeIntervalType.ONE_WEEK.description && cdpStatisticsList.list.size >= 7 || po.timeIntervalType == TimeIntervalType.ONE_MONTH.description && cdpStatisticsList.list.size >=30)
                    && cdpStatisticsList.list[cdpStatisticsList.list.size-1].time != DateTimeUtil.getToday())
                cdpStatisticsList.list.removeAt(0)

            if(cdpStatisticsList.list[cdpStatisticsList.list.size-1].time == DateTimeUtil.getToday()) cdpStatisticsList.list.removeAt(cdpStatisticsList.list.size - 1)
            val detail = CdpStatisticsVO()
            detail.cdpAddition = cdpStatis.cdpCreateCount
            detail.cdpClose = cdpStatis.cdpCloseCount
            detail.time = DateUtils.dateFormat(cdpStatis.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
            detail.price = cdpStatis.wiccPrice
            detail.cdpTotalActive = cdpStatis.cdpTotalActive
            cdpStatisticsList.list.add(detail)
        }else {
            val list = cdpStatisticsRepository.getCdpStats(po.startTime, po.endTime)
            list.let {
                for (bean in it) {
                    val detail = CdpStatisticsVO()
                    detail.cdpAddition = bean.cdpCreateCount
                    detail.cdpClose = bean.cdpCloseCount
                    detail.time = DateUtils.dateFormat(bean.statsTime, DateUtils.DATEPATTERN_YYYY_MM_DD)
                    detail.price = bean.wiccPrice
                    detail.cdpTotalActive = bean.cdpTotalActive
                    cdpStatisticsList.list.add(detail)
                }
            }
        }
        valueCacheRedisRepository.put(String.format(CdpRedisKeys.CDP_STATS_TOTAL_REDISKEY, po.timeIntervalType), JSONObject.toJSONString(cdpStatisticsList))
        return cdpStatisticsList
    }

    @Autowired
    lateinit var cdpStatisticsRepository: CdpStatsRepository

    @Autowired
    lateinit var delegateVoteWgrtTxRepository: DelegateVoteWgrtTxRepository

    @Autowired lateinit var valueCacheRedisRepository: ValueCacheRedisRepository

    private val logger  = LoggerFactory.getLogger(javaClass)

}