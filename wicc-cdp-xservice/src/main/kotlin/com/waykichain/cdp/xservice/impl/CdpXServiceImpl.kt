package com.waykichain.cdp.xservice.impl

import com.alibaba.fastjson.JSON
import com.google.gson.Gson
import com.waykichain.biz.redis.repository.impl.SetCacheRedisRepository
import com.waykichain.biz.redis.repository.impl.ValueCacheRedisRepository
import com.waykichain.cdp.commons.biz.dict.CdpOperateType
import com.waykichain.cdp.commons.biz.dict.CdpStatus
import com.waykichain.cdp.commons.biz.dict.CoinType
import com.waykichain.cdp.commons.biz.dict.UserCdpTxListType
import com.waykichain.cdp.commons.biz.domain.beans.CdpStatsInfo
import com.waykichain.cdp.commons.biz.domain.constants.CdpConstant
import com.waykichain.cdp.commons.biz.domain.constants.CdpConstant.BIGDECIMAL_ONE_YEAR_365_DAY
import com.waykichain.cdp.commons.biz.domain.constants.CdpRedisKeys
import com.waykichain.cdp.commons.biz.domain.po.*
import com.waykichain.cdp.commons.biz.domain.vo.*
import com.waykichain.cdp.commons.biz.env.Environment
import com.waykichain.cdp.commons.biz.service.CdpService
import com.waykichain.cdp.commons.biz.service.CdpTxService
import com.waykichain.cdp.commons.biz.util.AmountOperateUtil
import com.waykichain.cdp.commons.biz.util.sumByBigDecimal
import com.waykichain.cdp.util.MyBeanUtils
import com.waykichain.cdp.xservice.CdpXService
import com.waykichain.chain.dict.SysCoinType
import com.waykichain.chain.dict.TransactionType
import com.waykichain.chain.tx.CdpTxReceipt
import com.waykichain.chainstarter.service.WaykichainCoinHandler
import com.waykichain.coin.wicc.vo.tx.PriceMedianTx
import com.waykichain.commons.base.BizResponse
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.net.ConnectException
import java.util.*

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/24 15:26
 *
 * @Description:    cdp操作相关
 *
 */
@Service
class CdpXServiceImpl: CdpXService {

    override fun deleteNotify(ownerAddr:String, hash: String):BizResponse<String> {
        val res = setCacheRedisRepository.members(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+ownerAddr, String::class.java)?.map { Gson().fromJson(it, CdpLiquidateInfoVO::class.java) }
        setCacheRedisRepository.delete(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+ownerAddr)
        for(str in res!!) {
            if(str.hash != hash && !StringUtils.isEmpty(hash) && hash != "null") {
                val info = CdpLiquidateInfoVO()
                info.hash = str.hash
                info.liquidateTime = str.liquidateTime
                info.type = str.type
                info.cdpId = str.cdpId
                info.amount = str.amount
                setCacheRedisRepository.add(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+ownerAddr, Gson().toJson(info))
            }
        }
        return BizResponse()
    }

    /**
     * cdp detail
     */
    override fun userInfo(address: String): BizResponse<UserInfoDetailVO> {

        val vo  = UserInfoDetailVO()
        vo.address = address

        /* cdp info*/
        wiccCdpService.getByOwnerAddressAndStatus(address, CdpStatus.OPENING.code)?.let {

            val cdpInfoResult = chainHandler.getCdp(it.cdpId)

            if(cdpInfoResult.cdp == null) {
                //closed
                it.status = CdpStatus.CLOSED.code
                wiccCdpService.save(it)
            } else {
                vo.cdpInfo = CdpDetailVO()
                MyBeanUtils.copyProperties(it, vo.cdpInfo)
                vo.wiccAmount = it.bcoinAmount.stripTrailingZeros().toPlainString()
                vo.wusdAmount = it.scoinAmount.stripTrailingZeros().toPlainString()
                vo.cdpInfo!!.clearingPrice = it.liquidatePrice.stripTrailingZeros().toPlainString()
                vo.cdpInfo!!.createTxHash = it.cdpId
                vo.cdpInfo!!.initialedAt = it.cdpCreatedAt
                vo.cdpInfo!!.ownerAddress = it.ownerAddr
                vo.cdpInfo!!.wiccAmount = it.bcoinAmount.stripTrailingZeros().toPlainString()
                vo.cdpInfo!!.wusdAmount = it.scoinAmount.stripTrailingZeros().toPlainString()

                if (it.scoinAmount.compareTo(BigDecimal.ZERO) != 0) {
                    vo.cdpInfo!!.collateralRate = it.bcoinAmount.multiply(BigDecimal(getWiccPrice()))
                            .divide(it.scoinAmount.multiply(BigDecimal(CdpConstant.WUSD_PRICE_USD)), CdpConstant.BALANCE_DECIMAL_NUMBER_8, BigDecimal.ROUND_DOWN)
                            .multiply(CdpConstant.BIGDECIMAL_ONE_HUNDRED)
                            .setScale(8, BigDecimal.ROUND_DOWN)
                            .stripTrailingZeros().toPlainString()
                    val wusdAmount = it.scoinAmount.toDouble()
                    val h1 = cdpInfoResult.cdp.last_height
                    val h2 = chainHandler.getBlockCount() ?: 0
                    val up = wusdAmount * Math.ceil((h2 - h1) * 1.0 / CdpConstant.BLOCK_COUNT) / BIGDECIMAL_ONE_YEAR_365_DAY.toDouble()
                    val down = Math.log10(1 + CdpConstant.CDP_STABLE_FEE_B * wusdAmount)
                    vo.cdpInfo!!.fee = BigDecimal(up * 0.1 * CdpConstant.CDP_STABLE_FEE_A / down).setScale(8, BigDecimal.ROUND_UP)
                            .stripTrailingZeros().toPlainString()
                } else {
                    vo.cdpInfo!!.collateralRate = "--"
                    vo.cdpInfo!!.fee = "0"
                }
            }
        }
        if(vo.cdpInfo == null) {
            wiccCdpService.getLastCloseCdp(address, CdpStatus.CLOSED.code)?.let{
                vo.oldCdpInfo = CdpDetailVO()
                MyBeanUtils.copyProperties(it, vo.oldCdpInfo)
                vo.oldCdpInfo!!.clearingPrice = it.liquidatePrice.stripTrailingZeros().toPlainString()
                vo.oldCdpInfo!!.createTxHash = it.cdpId
                vo.oldCdpInfo!!.initialedAt = it.createdAt
                vo.oldCdpInfo!!.ownerAddress = it.ownerAddr
            }
        }
        try {
            /* amount*/
            chainHandler.getAccountInfo(address)?.let {
                if(it.tokens != null) {
                    vo.wiccAmount = AmountOperateUtil.getAmountFromLong(it.tokens[CoinType.WICC.msg]?.free_amount)
                    vo.wusdAmount = AmountOperateUtil.getAmountFromLong(it.tokens[CoinType.WUSD.msg]?.free_amount)
                }
            }

        }catch (e: ConnectException) {
            throw e
        }
        catch (e: Exception) {
            logger.error("Get balance from chain error! address=$address.", e)
        }

        val set =  setCacheRedisRepository.members(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+address,String::class.java)
        vo.liquidateInfo = set?.map { Gson().fromJson(it, CdpLiquidateInfoVO::class.java) }?.maxBy { it.liquidateTime }

        return BizResponse(vo)
    }

    /**
     * cdp global info
     */
    override fun globalInfo(): BizResponse<CdpGlobalInfoVO> {

        val vo = CdpGlobalInfoVO()
        vo.liquateRate = Environment.LIQUATE_RATE
        vo.penalty = Environment.PENALTY
        vo.wusdFee = Environment.WUSDFEE
        /* price*/
        vo.wiccPrice = getWiccPrice()
        vo.wusdPrice = CdpConstant.WUSD_PRICE_USD
        /* cdp info*/
        Gson().fromJson(valueCacheRedisRepository.get(CdpRedisKeys.CDP_STATS_INFO_REDISKEY), CdpStatsInfo::class.java)?.let {
            vo.cdpTotalWiccAmount = it.totalWiccAmount.stripTrailingZeros().toPlainString()
            vo.cdpTotalWusdAmount = it.totalWusdAmount.setScale(8, BigDecimal.ROUND_DOWN).stripTrailingZeros().toPlainString()
            vo.cdpTotalCount = it.totalCdpAmount
            if(it.totalCollateralRatio == "INF")vo.cdpTotalCollateralRatio = "-"
            else vo.cdpTotalCollateralRatio = it.totalCollateralRatio.replace("%", "").toBigDecimal().stripTrailingZeros().toPlainString().plus("%")
        }
        return BizResponse(vo)
    }

    override fun getWiccPrice(): String {
        val priceInfo  = Gson().fromJson(valueCacheRedisRepository.get(CdpRedisKeys.NEWEST_MEDIAN_PRICE_REDISKEY), PriceMedianTx::class.java)
        var wiccPrice = CdpConstant.ZERO_STRING
        priceInfo?.median_price_points?.firstOrNull { it.coin_symbol == CoinType.WICC.msg && it.price_symbol == CoinType.USD.msg }?.price?.let {
            wiccPrice = BigDecimal(it).divide(CdpConstant.BIGDECIMAL_10_POW_8, CdpConstant.BALANCE_DECIMAL_NUMBER_8,BigDecimal.ROUND_DOWN).toPlainString()
        }
        return wiccPrice
    }

    /**
     * cdp transactions
     */
    override fun cdpTxList(po: CdpTxListPO): BizResponse<CdpTxListVO> {

        val vo = CdpTxListVO()
        vo.totalpages = 0
        vo.totalcount = 0
        vo.currentpage = po.currentpage
        vo.type = po.type
        if(!StringUtils.isEmpty(po.cdpId)) {
            po.cdpIds.add(po.cdpId!!)
        }
        else {
            val cdps = wiccCdpService.getByOwnerAddress(po.address!!)
            when (po.type) {
                UserCdpTxListType.PRESENT.code -> {
                    po.cdpIds = cdps.filter { it.status == CdpStatus.OPENING.code }.map { it.cdpId }.toMutableList() as ArrayList<String>
                }
                UserCdpTxListType.HISTORY.code -> {
                    po.cdpIds = cdps.filter { it.status == CdpStatus.CLOSED.code }.map { it.cdpId }.toMutableList() as ArrayList<String>
                }
                else -> {
                    po.cdpIds = cdps.map { it.cdpId }.toList() as ArrayList<String>
                }
            }
        }
        if (po.cdpIds == null || po.cdpIds!!.isEmpty()) return BizResponse(vo)
        val result = wiccCdTxService.queryByPageAndCdpIds(po)

        vo.totalpages = result.totalPages
        vo.totalcount = result.totalElements.toInt()
        result.forEach {
            val detail = CdpTxDetailVO()
            MyBeanUtils.copyProperties(it, detail)

            detail.scoinAmount = it.scoinAmount?.stripTrailingZeros()?.toPlainString()?:"0"
            detail.bcoinAmount = it.bcoinAmount?.stripTrailingZeros()?.toPlainString()?:"0"
//            if(detail.scoinPenalty != null) detail.scoinPenalty = WiccUtils.convert(it.scoinPenalty.toLong()).divide(BigDecimal(CDP_MARKET_DISCOUNT).multiply(BigDecimal(getWiccPrice())),8, BigDecimal.ROUND_UP).stripTrailingZeros().toPlainString()
            detail.txHash = it.txid
            detail.confirmedAtMs = it.blockTimestamp
            when {
                it.txType == TransactionType.CDP_STAKE_TX.name && (it.fromStatus == CdpStatus.NULL.code || it.fromStatus ==0) -> detail.txType = CdpOperateType.CREATE.code
                it.txType == TransactionType.CDP_STAKE_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 1 && it.bcoinAmount.compareTo(BigDecimal.ZERO) == 0 -> detail.txType = CdpOperateType.LOAN_OUT.code
                it.txType == TransactionType.CDP_STAKE_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 0 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 1 -> detail.txType = CdpOperateType.PLEDGE.code
                it.txType == TransactionType.CDP_STAKE_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 1 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 1 -> detail.txType = CdpOperateType.PLEDGE_LOAN_OUT.code
                it.txType == TransactionType.CDP_STAKE_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 0 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 0 -> detail.txType = CdpOperateType.LOAN_OUT.code

                it.txType == TransactionType.CDP_REDEEM_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 1 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 0 -> detail.txType = CdpOperateType.REPAY.code
                it.txType == TransactionType.CDP_REDEEM_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 0 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 1 -> detail.txType = CdpOperateType.REDEEM.code
                it.txType == TransactionType.CDP_REDEEM_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 1 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 1 -> detail.txType = CdpOperateType.REPAY_REDEEM.code
                it.txType == TransactionType.CDP_REDEEM_TX.name && it.scoinAmount.compareTo(BigDecimal.ZERO) == 0 &&  it.bcoinAmount.compareTo(BigDecimal.ZERO) == 0 -> detail.txType = CdpOperateType.REPAY.code
                it.txType == TransactionType.CDP_LIQUIDATE_TX.name -> {
                    if (it.scoinPenalty==null)detail.scoinPenalty ="0"
                    detail.txType = CdpOperateType.LIQUATE.code
                    val receipts = JSON.parseArray(it.receipt, CdpTxReceipt::class.java)
                    var liquidatorAmount = BigDecimal.ZERO
                    var cdpOwnerAmount = BigDecimal.ZERO
                    if(receipts != null && receipts.isNotEmpty()) {
                        for (receipt in receipts) {
                            if (receipt.receipt_code?.toInt() == CdpConstant.CDP_ASSET_TO_LIQUIDATOR)
                                liquidatorAmount = receipt.coin_amount
                            if (receipt.receipt_code?.toInt() == CdpConstant.CDP_LIQUIDATED_ASSET_TO_OWNER)
                                cdpOwnerAmount = receipt.coin_amount
                        }
                    }

                    detail.assertValue = AmountOperateUtil.getAmountFromBigDecimal(liquidatorAmount)
                    if(StringUtils.isEmpty(it.txid))detail.assertValue = it.bcoinAmount.stripTrailingZeros().toPlainString()
                    detail.bcoinAmount = AmountOperateUtil.getAmountFromBigDecimal(cdpOwnerAmount)
                }
            }

            if(it.toStatus == CdpStatus.CLOSED.code) detail.txType = CdpOperateType.CLOSE.code
            vo.list.add(detail)
        }
        return BizResponse(vo)
    }



    /**----------------------------------------------------cdp market-----------------------------------------------------------------*/

    /**
     * cdp transactions
     */
    override fun cdpLdTxList(po: CdpLdTxListPO): BizResponse<CdpTxListVO> {

        val vo = CdpTxListVO()
        val result = wiccCdTxService.queryByPageAndLdAddr(po)
        vo.totalpages = result.totalPages
        vo.totalcount = result.totalElements.toInt()
        vo.currentpage = po.currentpage
        result.forEach {cdptx ->
            val detail = CdpTxDetailVO()
            MyBeanUtils.copyProperties(cdptx, detail)
            detail.scoinAmount = cdptx.toWusdAmount.stripTrailingZeros().toPlainString()
            detail.bcoinAmount = cdptx.toWiccAmount.stripTrailingZeros().toPlainString()
            detail.txHash = cdptx.txid
            detail.confirmedAtMs = cdptx.blockTimestamp
            detail.id = wiccCdpService.getByHash(detail.cdpId!!)!!.id
            if (cdptx.txType == TransactionType.CDP_LIQUIDATE_TX.name && StringUtils.isNotBlank(cdptx.receipt)) {

                val receipts = JSON.parseArray(cdptx.receipt, CdpTxReceipt::class.java)
                if (receipts.isNotEmpty() ) {
                    val wiccAmount = receipts.filter { it.coin_symbol == SysCoinType.WICC.code && (it.receipt_code?.toInt() == CdpConstant.CDP_ASSET_TO_LIQUIDATOR)}.sumByBigDecimal{ it.coin_amount}
                    val wusdAmount = receipts.filter { it.coin_symbol == SysCoinType.WUSD.code && it.receipt_code?.toInt() == CdpConstant.CDP_SCOIN_FROM_LIQUIDATOR}.sumByBigDecimal{it.coin_amount}
                    detail.payValue =   AmountOperateUtil.getAmountFromBigDecimal(wusdAmount)
                    detail.assertValue =  AmountOperateUtil.getAmountFromBigDecimal(wiccAmount)
                    detail.assertValueDollar = wusdAmount
                            .divide(BigDecimal(CdpConstant.CDP_MARKET_DISCOUNT)*CdpConstant.BIGDECIMAL_10_POW_8, 8, BigDecimal.ROUND_HALF_UP)
                            .stripTrailingZeros().toPlainString()
                }
            }
            vo.list.add(detail)
        }
        return BizResponse(vo)
    }



    override fun cdpLdList(po: CdpLdListPO): BizResponse<CdpListVO> {
        po.price = BigDecimal(getWiccPrice())
        val vo = CdpListVO()
        val result = wiccCdpService.queryByPageAndLdCR(po)
        vo.totalpages = result.totalPages
        vo.totalcount = result.totalElements.toInt()
        vo.currentpage = po.currentpage
        result.forEach {
            val detail = CdpDetailVO()
            MyBeanUtils.copyProperties(it, detail)
            detail.wiccAmount = it.bcoinAmount.stripTrailingZeros().toPlainString()
            detail.wusdAmount = it.scoinAmount.stripTrailingZeros().toPlainString()
            detail.clearingPrice = it.liquidatePrice.stripTrailingZeros().toPlainString()
            detail.createTxHash = it.cdpId
            detail.initialedAt = it.createdAt
            detail.ownerAddress = it.ownerAddr
            if(BigDecimal(detail.wusdAmount).compareTo(BigDecimal.ZERO)!=0)detail.collateralRate = BigDecimal(detail.wiccAmount).multiply(BigDecimal(getWiccPrice()))
                                    .divide(BigDecimal(detail.wusdAmount),8, BigDecimal.ROUND_UP).stripTrailingZeros().toPlainString()
            if(BigDecimal(detail.collateralRate) >CdpConstant.CDP_MARKET_COLLATERAL_RATIO_2 ) {
                detail.assertValue = BigDecimal(detail.wusdAmount).multiply(CdpConstant.CDP_MARKET_COLLATERAL_RATIO_2)
                                    .multiply(BigDecimal(CdpConstant.WUSD_PRICE_USD))
                                    .divide(BigDecimal(getWiccPrice()), 8, BigDecimal.ROUND_DOWN)
                detail.assertValueDollar = BigDecimal(detail.wusdAmount).multiply(CdpConstant.CDP_MARKET_COLLATERAL_RATIO_2)
                                            .multiply(BigDecimal(CdpConstant.WUSD_PRICE_USD)).setScale(8, BigDecimal.ROUND_DOWN)
                detail.payValue = detail.assertValueDollar!!.multiply(BigDecimal(CdpConstant.CDP_MARKET_DISCOUNT)).setScale(8,BigDecimal.ROUND_UP)
            } else if (BigDecimal(detail.collateralRate) >CdpConstant.CDP_MARKET_COLLATERAL_RATIO_1 && BigDecimal(detail.collateralRate) <= CdpConstant.CDP_MARKET_COLLATERAL_RATIO_2) {
                detail.assertValue = BigDecimal(detail.wiccAmount)
                detail.assertValueDollar =  BigDecimal(detail.wiccAmount).multiply(BigDecimal(getWiccPrice())).setScale(8, BigDecimal.ROUND_DOWN)
                detail.payValue = detail.assertValueDollar!!.multiply(BigDecimal(CdpConstant.CDP_MARKET_DISCOUNT)).setScale(8,BigDecimal.ROUND_UP)
            }
            detail.discount = CdpConstant.CDP_MARKET_DISCOUNT.toString()
            detail.collateralRate = BigDecimal(detail.collateralRate).multiply(BigDecimal(100)).toPlainString()
            vo.list.add(detail)
        }
        return BizResponse(vo)
    }

    override fun cdpList(po: CdpListPO): BizResponse<CdpListVO> {
        po.price = BigDecimal(getWiccPrice())
        val result = wiccCdpService.queryByStatus(po)
        val vo = CdpListVO()
        vo.totalpages = result.totalPages
        vo.totalcount = result.totalElements.toInt()
        vo.currentpage = po.currentpage
        result.forEach {
            var detail = CdpDetailVO()
            MyBeanUtils.copyProperties(it, detail)
            detail.clearingPrice = it.liquidatePrice.stripTrailingZeros().toPlainString()
            detail.status = it.status

            when {
                po.status == CdpStatus.CLOSED.code -> detail.status = CdpStatus.CLOSED.code
                BigDecimal(detail.clearingPrice).compareTo(po.price)<0 && detail.status!= CdpStatus.CLOSED.code -> detail.status = CdpStatus.SAFETY.code
                BigDecimal(detail.clearingPrice).compareTo(po.price)>=0 && detail.status!= CdpStatus.CLOSED.code -> detail.status = CdpStatus.UNSAFETY.code
            }


            detail.wiccAmount = it.bcoinAmount.stripTrailingZeros().toPlainString()
            detail.wusdAmount = it.scoinAmount.stripTrailingZeros().toPlainString()
            detail.clearingPrice = it.liquidatePrice.stripTrailingZeros().toPlainString()
            detail.createTxHash = it.cdpId
            detail.initialedAt = it.createdAt
            detail.ownerAddress = it.ownerAddr
            if(BigDecimal(detail.wusdAmount).compareTo(BigDecimal.ZERO)!=0)detail.collateralRate = BigDecimal(detail.wiccAmount).multiply(BigDecimal(getWiccPrice()))
                    .divide(BigDecimal(detail.wusdAmount),8, BigDecimal.ROUND_HALF_UP)
                    .divide(BigDecimal(CdpConstant.WUSD_PRICE_USD),8, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString()
            detail.collateralRate = BigDecimal(detail.collateralRate).multiply(BigDecimal(100)).toPlainString()
//            detail.updatedAt = it.updatedAt
            vo.list.add(detail)
        }
        return BizResponse(vo)
    }

    override fun cdpCloseList(po: CdpLdTxListPO): BizResponse<CdpListVO> {
        val result = wiccCdpService.queryCloseCdpList(po)
        val vo = CdpListVO()
        vo.totalpages = result.totalPages
        vo.totalcount = result.totalElements.toInt()
        vo.currentpage = po.currentpage
        result.forEach {
            val detail = CdpDetailVO()
            MyBeanUtils.copyProperties(it, detail)
            detail.createdAt = it.cdpCreatedAt
            detail.updatedAt = Date(wiccCdTxService.getLastCdpOperateTime(it.cdpId))
            vo.list.add(detail)
        }
       return BizResponse(vo)
    }

    override fun cdpLdPlanDetail(po: CdpLdPO): BizResponse<CdpPlanVO> {
        val vo = CdpPlanVO()
        val result = wiccCdTxService.getById(po.id!!)
        BeanUtils.copyProperties(result, vo)
        vo.collateralRate = result!!.fromWiccAmount.multiply(BigDecimal(getWiccPrice()))
                .divide(result.fromWusdAmount,2, BigDecimal.ROUND_HALF_UP).
                        divide(BigDecimal(CdpConstant.WUSD_PRICE_USD),2, BigDecimal.ROUND_HALF_UP)
                .stripTrailingZeros()
                .toPlainString()
        vo.wusdAmount = result.fromWiccAmount.stripTrailingZeros().toPlainString()
        vo.currentLdValue = result.fromWiccAmount.multiply(BigDecimal(getWiccPrice()))
                .stripTrailingZeros().toPlainString()
        vo.maxLdValue = BigDecimal(vo.currentLdValue).multiply(BigDecimal(CdpConstant.CDP_MARKET_DISCOUNT))
                     .stripTrailingZeros().toPlainString()
        vo.assets = vo.wusdAmount
        vo.assertValue = BigDecimal(vo.currentLdValue).stripTrailingZeros()
        vo.discount = CdpConstant.CDP_MARKET_DISCOUNT.toString()
        vo.ldDiscount = BigDecimal(vo.currentLdValue).multiply(BigDecimal(1 - CdpConstant.CDP_MARKET_DISCOUNT))
                .stripTrailingZeros().toPlainString()
        vo.fees = result.fees.stripTrailingZeros().toPlainString()
        return BizResponse(vo)
    }


    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var wiccCdpService: CdpService

    @Autowired
    lateinit var wiccCdTxService: CdpTxService

    @Autowired
    lateinit var chainHandler: WaykichainCoinHandler

    @Autowired lateinit var cdpXService: CdpXService

    @Autowired
    lateinit var valueCacheRedisRepository: ValueCacheRedisRepository

    @Autowired
    lateinit var setCacheRedisRepository: SetCacheRedisRepository

}