package com.waykichain.cdp.xservice

import com.waykichain.cdp.commons.biz.domain.po.*
import com.waykichain.cdp.commons.biz.domain.vo.*
import com.waykichain.commons.base.BizResponse

/**
 *
 * @Author:         yanjunlin
 *
 * @CreateDate:     2019/7/24 15:22
 *
 * @Description:    cdp操作相关
 *
 */
interface CdpXService {

    fun userInfo(address: String): BizResponse<UserInfoDetailVO>

    fun globalInfo(): BizResponse<CdpGlobalInfoVO>

    fun cdpTxList(po: CdpTxListPO): BizResponse<CdpTxListVO>

    fun cdpLdTxList(po: CdpLdTxListPO): BizResponse<CdpTxListVO>

    fun cdpLdList(po: CdpLdListPO): BizResponse<CdpListVO>

    fun cdpList(po: CdpListPO): BizResponse<CdpListVO>

    fun cdpCloseList(po: CdpLdTxListPO): BizResponse<CdpListVO>

    fun cdpLdPlanDetail(cdpId: CdpLdPO): BizResponse<CdpPlanVO>

    fun getWiccPrice(): String

    fun deleteNotify(ownerAddr:String, hash: String):BizResponse<String>
}