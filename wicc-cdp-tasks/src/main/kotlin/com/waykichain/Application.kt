package com.waykichain

import com.waykichain.chainstarter.config.TempConfigEnvironment
import com.waykichain.commons.util.BaseEnv
import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication(exclude=arrayOf( MongoAutoConfiguration::class, MongoDataAutoConfiguration::class))
@MapperScan("com/waykichain/waykicomm/commons/biz/mapper")
@EnableScheduling
open class Application

fun main(args: Array<String>) {
    TempConfigEnvironment.putConfig("WICC_HOST_IP", "127.0.0.1")
    TempConfigEnvironment.putConfig("WICC_HOST_PORT", "6968")
    TempConfigEnvironment.putConfig("WICC_RPC_PASSWORD" , "wicc1000")
    TempConfigEnvironment.putConfig("WICC_RPC_USERNAME", "wiccuser")
    BaseEnv.init()
    SpringApplication.run(Application::class.java, *args)

}