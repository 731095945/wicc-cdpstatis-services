package com.waykichain.cdp.task.jobs

import com.waykichain.cdp.xservice.StatisticsXService
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.JobHandler
import com.xxl.job.core.log.XxlJobLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@JobHandler("statisticsInfoJobHandler")
@Service
class StatisticsInfoJobHandler : IJobHandler() {
    // 1min refresh
    override fun execute(p0: String?): ReturnT<String> {
        XxlJobLogger.log("[Handler - update cdp_statistics_info] start --- >")
        statisticsXService.updateCdpInfo()
        statisticsXService.updateWiccInfo()
        statisticsXService.updateWusdInfo()
        statisticsXService.updateWgrtInfo()
        XxlJobLogger.log("[Handler - update cdp_statistics_info] end --- >")
        return SUCCESS
    }
    @Autowired
    lateinit var statisticsXService: StatisticsXService
}