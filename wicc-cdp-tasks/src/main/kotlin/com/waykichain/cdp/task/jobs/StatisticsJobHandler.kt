package com.waykichain.cdp.task.jobs

import com.waykichain.cdp.commons.biz.dict.TimeIntervalType
import com.waykichain.cdp.commons.biz.domain.po.CdpStatPO
import com.waykichain.cdp.xservice.StatisticsXService
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.JobHandler
import com.xxl.job.core.log.XxlJobLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@JobHandler("statisticsJobHandler")
@Service
class StatisticsJobHandler : IJobHandler() {
    //1 min refresh
    override fun execute(p0: String?): ReturnT<String> {
        XxlJobLogger.log("[Handler - update cdp_stats] start --- >")

        statisticsXService.updateCdpStatisDB()

        var cdpStatPO = CdpStatPO()
        for(t in TimeIntervalType.values()){
            try {
                cdpStatPO.timeIntervalType = t.description
                statisticsXService.updateWusd(cdpStatPO)
                statisticsXService.updateWicc(cdpStatPO)
                statisticsXService.updateCdp(cdpStatPO)
                statisticsXService.updateWgrt(cdpStatPO)
            }catch (e:Exception){
                logger.error("[StatisticsJobHandler error:]", e)
                XxlJobLogger.log("[StatisticsJobHandler error:]", e)
            }
        }

        XxlJobLogger.log("[Handler - update cdp_stats] end --- >")
        return SUCCESS
    }
    @Autowired
    lateinit var statisticsXService: StatisticsXService
    private val logger  = LoggerFactory.getLogger(javaClass)



}