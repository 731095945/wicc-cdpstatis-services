package com.waykichain.cdp.webapi.controller

import com.waykichain.cdp.task.jobs.CdpStatsJobHandler
import com.waykichain.cdp.task.jobs.StatisticsJobHandler
import com.waykichain.chainstarter.service.WaykichainCoinHandler
import com.waykichain.coin.wicc.vo.tx.CdpLiquidateTx
import com.waykichain.commons.base.BizResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/test")
@Api(description = "test")
class TestController {

    @Autowired lateinit var chainHandler: WaykichainCoinHandler

    @Autowired lateinit var  statsJobHandler: StatisticsJobHandler



    @GetMapping("/task")
    @ApiOperation("wusd每日产生和销毁", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    fun testTask() {
        statsJobHandler.execute("")
        //statsJobHandler.execute("a")
        //return BizResponse(cdpStatsJobHandler.execute("").msg)

//        val getCdpCloseInfoPO = GetCdpCloseInfoPO()
//        getCdpCloseInfoPO.cdpid = "83f6b0a4549d571635c43cf56fcea044640a72665853de4197c447f685dacebe"
//        val cdpCloseInfoResult = chainHandler.getCloseCdp(getCdpCloseInfoPO)
//        print("end")
//        return BizResponse()

//        val localHeight = 212431
//        val chainHeight = 212432

//        try {
//            for(i in localHeight+1.. chainHeight){
//                XxlJobLogger.log("Before synchronize the present height is: " + i)
//                val block = handler.getBlockByHeight(i)
//                handleTradeRequest(block)
//                valueCacheRedisRepository.put(CdpRedisKeys.CDP_TX_LOCAL_CHAIN_HEIGHT_REDIS_KEY, i.toString())
//                XxlJobLogger.log("After synchronize the present height is: " + i)
//            }
//        } catch (e: Exception) {
//            logger.error("Cdp chain error", e)
//            XxlJobLogger.log(e.toString())
//        }


    }

    @GetMapping("/addRedis")
    fun testAddRedis():BizResponse<String> {

        var tx: CdpLiquidateTx = CdpLiquidateTx()
        tx.cdp_txid = "111111"
        tx.confirmed_time = 9999
        var ownerAddr = "dfdfdf"

//        var res = setCacheRedisRepository.members(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+ownerAddr, String::class.java)?.map { Gson().fromJson(it, CdpLiquidateInfo::class.java) }


//        cdpLiquidateTxHandler.addLiquidateTx(tx, ownerAddr)
//        res = setCacheRedisRepository.members(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+ownerAddr, String::class.java)?.map { Gson().fromJson(it, CdpLiquidateInfo::class.java) }
//
//        tx.cdp_txid = "22222"
//        cdpLiquidateTxHandler.addLiquidateTx(tx, ownerAddr)
//        tx.cdp_txid = "33333"
//        cdpLiquidateTxHandler.addLiquidateTx(tx, ownerAddr)
       // cdpLiquidateTxHandler.clearLiquidateTx(tx, ownerAddr)
//         res = setCacheRedisRepository.members(CdpRedisKeys.CDP_LIQUIDATE_ADDRESS+ownerAddr, String::class.java)?.map { Gson().fromJson(it, CdpLiquidateInfo::class.java) }
        return BizResponse()
    }

    @Autowired
    lateinit var cdpStatsJobHandler: CdpStatsJobHandler

//    @Autowired
//    lateinit var cdpLiquidateTxHandler: CdpLiquidateTxHandler
//
//    @Autowired
//    lateinit var setCacheRedisRepository: SetCacheRedisRepository
}