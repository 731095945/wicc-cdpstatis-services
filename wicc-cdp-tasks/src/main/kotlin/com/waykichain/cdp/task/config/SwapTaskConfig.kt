package com.waykichain.swap.task.config

import com.xxl.job.core.executor.XxlJobExecutor
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("com.waykichain")
open class SwapTaskConfig : TaskExecutorConfig(){

    @Bean(initMethod = "start", destroyMethod = "destroy")
    open fun xxlJobExecutor(): XxlJobExecutor {
        return init()
    }

}