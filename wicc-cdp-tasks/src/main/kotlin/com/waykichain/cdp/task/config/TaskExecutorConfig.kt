package com.waykichain.swap.task.config

import com.waykichain.cdp.commons.biz.env.task.Environment
import com.xxl.job.core.executor.XxlJobExecutor
import org.slf4j.LoggerFactory

/**
 * Created by richardchen on 8/22/17.
 */
open class TaskExecutorConfig {
    private val logger = LoggerFactory.getLogger(javaClass)


    open fun init(): XxlJobExecutor {
        logger.trace("------------ xxlJobExecutor -----------")

        val xxlJobExecutor = XxlJobExecutor()
        xxlJobExecutor.setIp(Environment.TASK_EXECUTOR_IP)
        xxlJobExecutor.setPort(Integer.parseInt(Environment.TASK_EXECUTOR_PORT))
        xxlJobExecutor.setAppName(Environment.TASK_APP_NAME)
        xxlJobExecutor.setAdminAddresses(Environment.TASK_JOB_ADMIN_URL)
        xxlJobExecutor.setLogPath(Environment.TASK_LOG_PATH)
        return xxlJobExecutor
    }

}