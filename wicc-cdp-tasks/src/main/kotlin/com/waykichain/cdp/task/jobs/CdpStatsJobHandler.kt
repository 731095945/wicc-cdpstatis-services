package com.waykichain.cdp.task.jobs

import com.alibaba.fastjson.JSONObject
import com.waykichain.biz.redis.repository.impl.ValueCacheRedisRepository
import com.waykichain.cdp.commons.biz.domain.beans.CdpStatsInfo
import com.waykichain.cdp.commons.biz.domain.constants.CdpRedisKeys
import com.waykichain.cdp.commons.biz.util.AmountOperateUtil
import com.waykichain.chainstarter.service.WaykichainCoinHandler
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.JobHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

/**
 *  Created by yehuan on 2019/7/15
 */
@Service
@JobHandler("cdpStatsJobHandler")
class CdpStatsJobHandler:IJobHandler(){

    override fun execute(p0: String?): ReturnT<String> {

        val cdpStatsInfo = CdpStatsInfo()
        var wiccGetScoinInfoResult = chainHandler.getSCoinInfo()
        cdpStatsInfo.totalWiccAmount = BigDecimal(AmountOperateUtil.getAmountFromLong(wiccGetScoinInfoResult!!.global_staked_bcoins))
        cdpStatsInfo.totalWusdAmount = BigDecimal(AmountOperateUtil.getAmountFromLong(wiccGetScoinInfoResult.global_owed_scoins))
        cdpStatsInfo.forceLiquidateCdpAmount = wiccGetScoinInfoResult.force_liquidate_cdp_amount
        cdpStatsInfo.forceLiquidateRatio =  wiccGetScoinInfoResult.force_liquidate_ratio
        cdpStatsInfo.globalCollateralCeiling = wiccGetScoinInfoResult.global_collateral_ceiling
        cdpStatsInfo.totalCollateralRatio = wiccGetScoinInfoResult.global_collateral_ratio
        cdpStatsInfo.globalCollateralRatioFloor = wiccGetScoinInfoResult.global_collateral_ratio_floor
        cdpStatsInfo.globalCollateralRatioFloorReached = wiccGetScoinInfoResult.global_collateral_ratio_floor_reached
        cdpStatsInfo.height = wiccGetScoinInfoResult.tipblock_height
        cdpStatsInfo.slideWindowBlockCount = wiccGetScoinInfoResult.slide_window_block_count

        valueCacheRedisRepository.put(CdpRedisKeys.CDP_STATS_INFO_REDISKEY, JSONObject.toJSONString(cdpStatsInfo))

        return SUCCESS
    }

    @Autowired lateinit var valueCacheRedisRepository: ValueCacheRedisRepository
    @Autowired lateinit var chainHandler: WaykichainCoinHandler

}